package sgo.mobile.mava.conf;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class AplConstants {

    public static Boolean IS_PROD = false;
    public static final int HTTP_DEFAULT_TIMEOUT = 60 * 1000;
    public static final int HTTP_LONG_TIMEOUT = 240 * 1000;
    public static final String CallbackURL = "http://116.90.162.173:20080/";

//    public static final String headaddressDEV = "http://116.90.162.173:59088/kwsg/";
    public static final String headaddressDEV = "https://mobile-dev.espay.id/kwsg/";
//    public static final String headaddressDEV = "https://mobile.espay.id/kwsg/";

    public static final String SignSalesMobileAPI  = headaddressDEV + "SalesLogin/SignIn";
    public static final String SalesForgotPassMobileAPI    = headaddressDEV + "SalesForgotPassword/Invoke";
    public static final String SalesChangePassMobileAPI    = headaddressDEV + "SalesChangePassword/Invoke";
    public static final String DiCommunityMobileAPI  = headaddressDEV + "DigInclusionCommunity/Invoke";
    public static final String DiMemberMobileAPI  = headaddressDEV + "DigInclusionMember/Retrieve";
    public static final String DiListMobileAPI  = headaddressDEV + "DigInclusionList/Retrieve";
    public static final String DiListPayMobileAPI  = headaddressDEV + "DigInclusionList/RetrievePay";
    public static final String DiSavePayMobileAPI  = headaddressDEV + "DigInclusionList/Invoke";
    public static final String DiValidateMobilAPI = headaddressDEV + "DigInclusionList/Validate";
    public static final String DiPaymentMobilAPI = headaddressDEV + "DigInclusionPayment/Validate";
    public static final String CommunityInvMobileAPI  = headaddressDEV + "InvCommunity/Invoke";
    public static final String MemberInvMobileAPI     = headaddressDEV + "InvMember/Retrieve";
    public static final String InvListMobileAPI     = headaddressDEV + "InvList/Retrieve";
    public static final String InvListPayMobileAPI  = headaddressDEV + "InvList/RetrievePay";
    public static final String InvValidateMobilAPI = headaddressDEV + "InvList/Validate";
    public static final String InvSavePayMobileAPI  = headaddressDEV + "InvList/Invoke";
    public static final String ReqTokenInvPaymentMobileAPI  = headaddressDEV + "ReqTokenInv/Invoke";
    public static final String ConfirmTokenInvPaymentMobileAPI  = headaddressDEV + "ConfirmTokenInv/Invoke";
    public static final String PaymentReportInvPaymentMobileAPI    = headaddressDEV + "InvPayment/Invoke";
    public static final String CommunityInvUserMobileAPI  = headaddressDEV + "DgiPaymentCommunity/Retrieve";
    public static final String MemberInvUserMobileAPI     = headaddressDEV + "DgiPaymentMember/Retrieve";
    public static final String InvUserListMobileAPI     = headaddressDEV + "DgiPaymentInv/Retrieve";
    public static final String InvUserListPayMobileAPI  = headaddressDEV + "InvListUser/RetrievePay";
    public static final String InvValidatUsereMobilAPI = headaddressDEV + "DgiPaymentInv/Validate";
    public static final String InvUserSavePayMobileAPI  = headaddressDEV + "DgiPaymentInv/Invoke";
    public static final String PaymentReportInvUserMobileAPI    = headaddressDEV + "InvPaymentUser/Invoke";
    public static final String BankCCLMobileAPI  = headaddressDEV + "CashCollectionBank/Retrieve";
    public static final String CommunityCCLMobileAPI  = headaddressDEV + "CommunityCCL/Retrieve";
    public static final String MemberCCLMobileAPI     = headaddressDEV + "CashCollectionMember/Retrieve";
    public static final String MemberCCLDIMobileAPI     = headaddressDEV + "DiCCLMember/Retrieve";
    public static final String ConfirmTokenDiMobileAPI  = headaddressDEV + "ConfirmTokenDigInclusion/Invoke";
    public static final String ResendTokenDiMobileAPI  = headaddressDEV + "ResendTokenDigInclusion/Invoke";
    public static final String ValidateCCLMobileAPI      = headaddressDEV + "CashCollectionValidate/Invoke";
    public static final String PaymentReportCCLMobileAPI      = headaddressDEV + "CashCollectionPayment/Invoke";
    public static final String DepositCCLMobileAPI      = headaddressDEV + "ReqTokenCashCollection/Invoke";
    public static final String RequestTokenCCLMobileAPI    = headaddressDEV + "ReqTokenCashCollection/Invoke";
    public static final String ConfirmTokenCCLMobileAPI    = headaddressDEV + "ConfirmTokenCashCollection/Invoke";
    public static final String DiReportCommunity    = headaddressDEV + "DiReportCommunity/Retrieve";
    public static final String DiReportMaster    = headaddressDEV + "DiReportMaster/Retrieve";
    public static final String DiReportDetail    = headaddressDEV + "DiReportDetail/Retrieve";
    public static final String ListMenuLCSMobileAPI = headaddressDEV + "ServiceMenuSetting/GetMenuSettingLCS";
    public static final String ForgotPassMobileAPI    = headaddressDEV + "MemberForgotPassword/Invoke";
    public static final String ChangePassMobileAPI    = headaddressDEV + "MemberChangePassword/Invoke";
    public static final String CommunityDiCCLMobileAPI     = headaddressDEV + "DiCCLCommunity/Retrieve";
    public static final String DiCCLListMobileAPI     = headaddressDEV + "DiCCLList/Retrieve";
    public static final String RequestTokenCCLDIMobileAPI    = headaddressDEV + "ReqTokenDiCCL/Invoke";
    public static final String ConfirmTokenCCLDIMobileAPI    = headaddressDEV + "ConfirmTokenDiCCL/Invoke";
    public static final String RequestTokenCCLDIRandomMobileAPI    = headaddressDEV + "ReqTokenDiCCLRandom/Invoke";
    public static final String ConfirmTokenCCLDIRandomMobileAPI    = headaddressDEV + "ConfirmTokenDiCCLRandom/Invoke";
    public static final String ValidateCCLDIMobileAPI      = headaddressDEV + "DiCCLValidate/Invoke";
    public static final String PaymentReportCCLDIMobileAPI      = headaddressDEV + "DigInclusionPayment/Invoke";
    public static final String SignMobileAPI  = headaddressDEV + "MemberLogin/SignIn";
    public static final String PulsaMobileAPI = headaddressDEV + "Transaction/telco";
    public static final String TopUpMobileAPI = headaddressDEV + "Transaction/TopUp";
    public static final String CashInC2CMobileAPI          = headaddressDEV + "Cash2Cash/CashIn";
    public static final String ConfirmTokenC2CMobileAPI    = headaddressDEV + "Cash2Cash/ConfirmToken";
    public static final String CashOutC2CMobileAPI         = headaddressDEV + "Cash2Cash/CashOut";
    public static final String DepositC2AMobileAPI      = headaddressDEV + "Cash2Account/Deposit";
    public static final String ConfirmTokenC2AMobileAPI    = headaddressDEV + "Cash2Account/ConfirmToken";
    public static final String DenomMobileAPI = headaddressDEV + "Transaction/Denom";
    public static final String RequestTokenEw9      = headaddressDEV + "ReqTokenEw9/Invoke";
    public static final String ConfirmTokenEw9      = headaddressDEV + "ConfirmTokenEw9/Invoke";
    public static final String ListInvoiceMobileAPI = headaddressDEV + "ServiceListInvoices/Retrieve";
    public static final String PayInvoiceMobileAPI  = headaddressDEV + "ServicePayInvoice/Invoke";
    public static final String TokenInvoiceMobileAPI = headaddressDEV + "TokenInvoice/Invoke";
    public static final String ReqTokenInvoiceMobileAPI = headaddressDEV + "ReqTokenSms/Invoke";
    public static final String ListAmountTrxMobileAPI = headaddressDEV + "ServiceAmountTrx/Retrieve";
    public static final String ListMenuSettingMobileAPI = headaddressDEV + "ServiceMenuSetting/GetAllMenuSetting";
    public static final String CommunityListMobileAPI = headaddressDEV + "ServiceCommunity/Retrieve";
    public static final String MemberListMobileAPI = headaddressDEV + "ServiceMember/Retrieve";
    public static final String BankListMobileAPI = headaddressDEV + "ServiceBank/Retrieve";
    public static final String BankProductMobileAPI = headaddressDEV + "ServiceBank/BankProduct";
    public static final String InquiryTopUpMobileAPI  = headaddressDEV + "InquiryTopUp/Retrieve";
    public static final String TopUpMemberMobileAPI   = headaddressDEV + "TopUpMember/Retrieve";
    public static final String TopUpPaymentMobileAPI  = headaddressDEV + "TopUpPayment/Invoke";
    public static final String TopUpValidateMobileAPI = headaddressDEV + "TopUpValidate/Invoke";
    public static final String ServiceApp = headaddressDEV + "ServiceApp/getAppVersion";
    public static final String DGIPaymentAPI = headaddressDEV + "DgiPayment/Invoke";
    public static final String TrxStatusAPI = headaddressDEV + "TrxStatus/Retrieve";
    public static final String InquiryTrxAPI = headaddressDEV + "InquiryTrx/Retrieve";
    public static final String InsertTrxAPI = headaddressDEV + "InsertTrx/Invoke";
    public static final String PaymentMemberAPI = headaddressDEV + "PaymentMember/Invoke";
    public static final String CommunityKMKAPI = headaddressDEV + "CommunityKMK/Retrieve";
    public static final String BankKMKAPI = headaddressDEV + "BankKMK/Retrieve";
    public static final String InvKMKAPI = headaddressDEV + "InvKMK/Retrieve";
    public static final String RepayInvKMKAPI = headaddressDEV + "RepayInvKMK/Invoke";
    public static final String InquiryTrxKMKAPI = headaddressDEV + "InquiryTrxKMK/Retrieve";
    public static final String ReqTokenKMKAPI = headaddressDEV + "ReqTokenKMK/Invoke";
    public static final String ConfirmTokenKMKAPI = headaddressDEV + "ConfirmTokenKMK/Invoke";
    public static final String AcctMandiriKUM = headaddressDEV + "AcctMandiriKUM/Retrieve";
    public static final String LimitMandiriKUM = headaddressDEV + "LimitMandiriKUM/Retrieve";
    public static final String DiReportEspay = headaddressDEV + "DiReportEspay/Retrieve";
    public static final String LINK_INQUIRY_SMS = headaddressDEV + "InquirySMS/Retrieve";

    //#################################################################################################################//

    public static final String APP_ID = "KWSG";
    //public static final String CommCodeCTA   =  "CTA0003";
    public static final String CommCodeCTA   =  "CTASGO6";
    //public static final String MemberCodeCTA =  "CTA1";
    public static final String MemberCodeCTA =  "ESPCash2Account";

    public static final String CommCodeCTC   =  "CTCSGO6";
    //public static final String CommCodeCTC   =  "CTCALFA";
    public static final String MemberCodeCTC =  "ESPCash2Cash2";
    //public static final String MemberCodeCTC =  "CTCALFA002";

    public static final String CommCodeCCL   = "CCL";
    public static final String MemberCodeCCL = "NORASEVELCCL";
    public static final String SchemeCodeCCL = "CCL";
    public static final String SenderIdCCL   = "MOBILE";

    public static String INCOMINGSMS_SPRINT = "+6281333332000";

    //#################################################################################################################//

    public static final String SalesIdInv  = "RAMLANGUST1392238684Y6MMN";
    public static final String MemberIdInv = "123";
    public static final String AreaInv     = "Jakarta";

    //public static final String NoIncomingToken = "6281382555096";
    public static final String NoIncomingToken = "6281514407650";
    //------------------------------------------------------------------------------------------------------------------//

    public static final int HTTP_TIMEOUT = 20 * 1000;

    public static final String MIN_AMOUNT_PAY = "1";
    public static final String MAX_AMOUNT_PAY = "2000000000";

    public AplConstants(){}

    public static String getCurrentDate(){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd", new Locale("id","INDONESIA"));
        return df.format(Calendar.getInstance().getTime());
    }

    public static String getCurrentDate(int minus){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd", new Locale("id","INDONESIA"));
        Calendar calendar=Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, -minus);
        return df.format(calendar.getTime());
    }

}

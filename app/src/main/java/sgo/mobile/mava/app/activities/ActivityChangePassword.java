package sgo.mobile.mava.app.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.mava.AppHelper;
import sgo.mobile.mava.R;
import sgo.mobile.mava.app.ui.dialog.DefinedDialog;
import sgo.mobile.mava.conf.AplConstants;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpClient;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpResponseHandler;
import sgo.mobile.mava.frameworks.net.loopj.android.http.MySSLSocketFactory;
import sgo.mobile.mava.frameworks.net.loopj.android.http.RequestParams;


public class ActivityChangePassword extends SherlockActivity {

    private Button btnDone;
    private Button btnBatal;

    private ProgressDialog pDialog;

    private EditText txtUserID;
    private EditText txtOldPassword;
    private EditText txtNewPassword;
    private EditText txtConfirmPassword;
    private String user_id, old_passsword, new_password, confirm_password;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        actionBarSetup();

        btnBatal    = (Button) findViewById(R.id.btnCancel);
        btnBatal.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                finish();
            }
        });

        findViews();
    }

    private void findViews(){
        btnDone       = (Button) findViewById(R.id.btnNext);
        txtUserID     = (EditText) findViewById(R.id.txtUserID);
        txtUserID.setText(AppHelper.getUserPhone(this));
        txtUserID.setKeyListener(null);
        txtOldPassword     = (EditText) findViewById(R.id.inpOldPassword);
        txtNewPassword     = (EditText) findViewById(R.id.inpNewPassword);
        txtConfirmPassword = (EditText) findViewById(R.id.inpConfirmPassword);

        btnDone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                user_id           = txtUserID.getText().toString();
                old_passsword     = txtOldPassword.getText().toString();
                new_password      = txtNewPassword.getText().toString();
                confirm_password  = txtConfirmPassword.getText().toString();

                if(user_id.equalsIgnoreCase("") || old_passsword.equalsIgnoreCase("") || new_password.equalsIgnoreCase("") || confirm_password.equalsIgnoreCase(""))
                {
                    Toast.makeText(getApplicationContext(), R.string.form_alert, Toast.LENGTH_SHORT).show();
                }else {

                   if(confirm_password.equals(new_password)) {
                       pDialog = DefinedDialog.CreateProgressDialog(ActivityChangePassword.this, pDialog, "Mengganti Password...");
                       AsyncHttpClient client = new AsyncHttpClient();
                       client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
                       RequestParams params = new RequestParams();

                       String loginType = AppHelper.getUserType(ActivityChangePassword.this);
                       ;
                       if (loginType.equalsIgnoreCase("Member / Agent")) {
                           params.put("user_id", user_id);
                           params.put("old_password", old_passsword);
                           params.put("new_password", new_password);
                           client.post(AplConstants.ChangePassMobileAPI, params, new AsyncHttpResponseHandler() {
                               public void onSuccess(String content) {
                                   Log.d("result:", content);
                                   try {
                                       JSONObject object = new JSONObject(content);

                                       String error_code = object.getString("error_code");
                                       String error_msg = object.getString("error_message");

                                       if (pDialog != null) {
                                           pDialog.dismiss();
                                       }

                                       if (error_code.equals("0000")) {

                                           if (pDialog != null) {
                                               pDialog.dismiss();
                                           }

                                           AlertDialog.Builder alert = new AlertDialog.Builder(ActivityChangePassword.this);
                                           alert.setTitle("Change Password");
                                           alert.setMessage("Change Password : Sukses");
                                           alert.setCancelable(false);
                                           alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                               @Override
                                               public void onClick(DialogInterface dialog, int which) {
                                                   finish();
                                               }
                                           });
                                           alert.show();

                                       } else {
                                           AlertDialog.Builder alert = new AlertDialog.Builder(ActivityChangePassword.this);
                                           alert.setTitle("Change Password");
                                           alert.setMessage("Change Password : " + error_msg);
                                           alert.setPositiveButton("OK", null);
                                           alert.show();
                                       }
                                   } catch (JSONException e) {
                                       // TODO Auto-generated catch block
                                       e.printStackTrace();

                                   }
                               }

                               ;

                               public void onFailure(Throwable error, String content) {
                                   if (pDialog != null) {
                                       pDialog.dismiss();
                                   }
                                   Toast.makeText(getApplicationContext(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                               }

                               ;
                           });
                       } else {
                           String param_sales_id = AppHelper.getUserId(ActivityChangePassword.this);
                           params.put("sales_id", param_sales_id);
                           params.put("old_password", old_passsword);
                           params.put("new_password", new_password);
                           client.post(AplConstants.SalesChangePassMobileAPI, params, new AsyncHttpResponseHandler() {
                               public void onSuccess(String content) {
                                   Log.d("result:", content);
                                   try {
                                       JSONObject object = new JSONObject(content);

                                       String error_code = object.getString("error_code");
                                       String error_msg = object.getString("error_message");

                                       if (pDialog != null) {
                                           pDialog.dismiss();
                                       }

                                       if (error_code.equals("0000")) {

                                           if (pDialog != null) {
                                               pDialog.dismiss();
                                           }

                                           AlertDialog.Builder alert = new AlertDialog.Builder(ActivityChangePassword.this);
                                           alert.setTitle("Change Password");
                                           alert.setMessage("Change Password : Sukses");
                                           alert.setCancelable(false);
                                           alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                               @Override
                                               public void onClick(DialogInterface dialog, int which) {
                                                   finish();
                                               }
                                           });
                                           alert.show();

                                       } else {
                                           AlertDialog.Builder alert = new AlertDialog.Builder(ActivityChangePassword.this);
                                           alert.setTitle("Change Password");
                                           alert.setMessage("Change Password : " + error_msg);
                                           alert.setPositiveButton("OK", null);
                                           alert.show();
                                       }
                                   } catch (JSONException e) {
                                       // TODO Auto-generated catch block
                                       e.printStackTrace();

                                   }
                               }

                               ;

                               public void onFailure(Throwable error, String content) {
                                   if (pDialog != null) {
                                       pDialog.dismiss();
                                   }
                                   Toast.makeText(getApplicationContext(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                               }

                               ;
                           });
                       }
                   }else{
                       Toast.makeText(getApplicationContext(), R.string.confirm_password_alert, Toast.LENGTH_SHORT).show();
                   }
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getSupportMenuInflater();
        inflater.inflate(R.menu.mainmenu, menu);
        return true;
    }

    protected void actionBarSetup()
    {
//        ActionBar actionBar = getSupportActionBar();
//        actionBar.setDisplayShowTitleEnabled(true);
//        actionBar.setDisplayHomeAsUpEnabled(true);
//        actionBar.setTitle(R.string.ab_title);
//        actionBar.setSubtitle(R.string.ab_subtitle);
//        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
//            actionBar.setHomeButtonEnabled(true);
//        }

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayUseLogoEnabled(false);
        actionBar.setDisplayShowHomeEnabled(false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            actionBar.setHomeButtonEnabled(true);
        }

        LayoutInflater inflator = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflator.inflate(R.layout.toolbar_main, null);

        actionBar.setCustomView(v);
        actionBar.setDisplayShowCustomEnabled(true);
    }


}
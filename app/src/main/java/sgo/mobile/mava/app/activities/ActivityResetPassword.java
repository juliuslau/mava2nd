package sgo.mobile.mava.app.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;
import com.actionbarsherlock.app.SherlockActivity;

import sgo.mobile.mava.conf.AplConstants;
import sgo.mobile.mava.frameworks.net.loopj.android.http.MySSLSocketFactory;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.mava.R;
import sgo.mobile.mava.app.ui.dialog.DefinedDialog;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpClient;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpResponseHandler;
import sgo.mobile.mava.frameworks.net.loopj.android.http.RequestParams;


public class ActivityResetPassword extends SherlockActivity {

    private RadioGroup OptGroupUserType;
    private RadioButton radioLogin;

    private Button btnDone;
    private Button btnBatal;

    private ProgressDialog pDialog;

    private EditText txtUserID;
    private String user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        btnBatal    = (Button) findViewById(R.id.btnCancel);
        btnBatal.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                finish();
            }
        });

        findViews();
    }

    private void findViews(){
        btnDone     = (Button) findViewById(R.id.btnNext);
        txtUserID   = (EditText) findViewById(R.id.txtUserID);

        btnDone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                user_id    = txtUserID.getText().toString();

                if(user_id.equalsIgnoreCase("") )
                {
                    Toast.makeText(getApplicationContext(), R.string.form_alert, Toast.LENGTH_SHORT).show();
                }else {
                    pDialog = DefinedDialog.CreateProgressDialog(ActivityResetPassword.this, pDialog, "Me-reset Password...");
                    AsyncHttpClient client = new AsyncHttpClient();
                    client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
                    RequestParams params = new RequestParams();

                    OptGroupUserType = (RadioGroup) findViewById(R.id.OptGroupUserType);

                    // get selected radio button from radioGroup
                    int selectedId = OptGroupUserType.getCheckedRadioButtonId();

                    // find the radiobutton by returned id
                    radioLogin = (RadioButton) findViewById(selectedId);
                    String loginType = radioLogin.getText().toString();
                    if (loginType.equalsIgnoreCase("Member / Agent")) {
                        params.put("user_id", user_id);
                        client.post(AplConstants.ForgotPassMobileAPI, params, new AsyncHttpResponseHandler() {
                            public void onSuccess(String content) {
                                Log.d("result:", content);
                                try {
                                    JSONObject object         = new JSONObject(content);

                                    String error_code         = object.getString("error_code");
                                    String error_msg          = object.getString("error_message");

                                    if (pDialog != null) {
                                        pDialog.dismiss();
                                    }

                                    if (error_code.equals("0000")) {

                                        if (pDialog != null) {
                                            pDialog.dismiss();
                                        }

                                        AlertDialog.Builder alert = new AlertDialog.Builder(ActivityResetPassword.this);
                                        alert.setTitle("Reset Password");
                                        alert.setMessage("Reset Password : Sukses");
                                        alert.setPositiveButton("OK", null);
                                        alert.show();

                                    } else {
                                        AlertDialog.Builder alert = new AlertDialog.Builder(ActivityResetPassword.this);
                                        alert.setTitle("Reset Password");
                                        alert.setMessage("Reset Password : " + error_msg);
                                        alert.setPositiveButton("OK", null);
                                        alert.show();
                                    }
                                } catch (JSONException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();

                                }
                            };

                            public void onFailure(Throwable error, String content) {
                                if (pDialog != null) {
                                    pDialog.dismiss();
                                }
                                Toast.makeText(getApplicationContext(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                            }

                            ;
                        });
                    } else {
                        params.put("sales_code", user_id);

                        client.post(AplConstants.SalesForgotPassMobileAPI, params, new AsyncHttpResponseHandler() {
                            public void onSuccess(String content) {
                                Log.d("result:", content);
                                try {
                                    JSONObject object         = new JSONObject(content);

                                    String error_code         = object.getString("error_code");
                                    String error_msg          = object.getString("error_message");

                                    if (pDialog != null) {
                                        pDialog.dismiss();
                                    }

                                    if (error_code.equals("0000")) {

                                        if (pDialog != null) {
                                            pDialog.dismiss();
                                        }

                                        AlertDialog.Builder alert = new AlertDialog.Builder(ActivityResetPassword.this);
                                        alert.setTitle("Reset Password");
                                        alert.setMessage("Reset Password : Sukses");
                                        alert.setPositiveButton("OK", null);
                                        alert.show();

                                    } else {
                                        AlertDialog.Builder alert = new AlertDialog.Builder(ActivityResetPassword.this);
                                        alert.setTitle("Reset Password");
                                        alert.setMessage("Reset Password : " + error_msg);
                                        alert.setPositiveButton("OK", null);
                                        alert.show();
                                    }
                                } catch (JSONException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();

                                }
                            };

                            public void onFailure(Throwable error, String content) {
                                if (pDialog != null) {
                                    pDialog.dismiss();
                                }
                                Toast.makeText(getApplicationContext(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                            }

                            ;
                        });
                    }
                }
            }
        });
    }


}
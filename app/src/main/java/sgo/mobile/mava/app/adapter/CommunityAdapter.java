package sgo.mobile.mava.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import sgo.mobile.mava.R;
import sgo.mobile.mava.app.beans.CommunityModel;

/**
 * Created by thinkpad on 1/19/2016.
 */
public class CommunityAdapter extends BaseAdapter {
    List<CommunityModel> data;
    Activity activity;

    public CommunityAdapter(Activity _activity, List<CommunityModel> _data){
        data = _data;
        activity = _activity;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolder holder;

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.fragment_community_list_item, null);
            holder = new ViewHolder();

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.txtText    = (TextView) convertView.findViewById(R.id.txtText);
        holder.txtSubText = (TextView) convertView.findViewById(R.id.txtSubText);

        holder.txtText.setText(data.get(position).getName());
        holder.txtSubText.setText(data.get(position).getCode());

        return convertView;
    }

    static class ViewHolder {
        TextView txtText, txtSubText;
    }

}

package sgo.mobile.mava.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import sgo.mobile.mava.R;
import sgo.mobile.mava.app.fragments.FragmentConfirmRandom;
import sgo.mobile.mava.frameworks.math.FormatCurrency;

public class ConfirmRandomAdapter extends BaseAdapter {
    private Activity activity;

    public ConfirmRandomAdapter(Activity act) {
        this.activity = act;
    }
    public int getCount() {
        // TODO Auto-generated method stub
        return FragmentConfirmRandom.hold_id.size();
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolder holder;

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.fragment_random_confirm_item, null);
            holder = new ViewHolder();

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        Double dbl_amount = Double.parseDouble(FragmentConfirmRandom.amount.get(position));
        int int_amount = dbl_amount.intValue();

        holder.txtText    = (TextView) convertView.findViewById(R.id.txtText);
        holder.txtSubText = (TextView) convertView.findViewById(R.id.txtSubText);

        holder.txtText.setText(FragmentConfirmRandom.member_shop.get(position));
        holder.txtSubText.setText(FormatCurrency.getRupiahFormat(Integer.toString(int_amount)));

        return convertView;
    }

    static class ViewHolder {
        TextView txtText, txtSubText;
    }
}

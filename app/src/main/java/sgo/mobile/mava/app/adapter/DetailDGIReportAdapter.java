package sgo.mobile.mava.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import sgo.mobile.mava.R;
import sgo.mobile.mava.app.fragments.FragmentDetailReportDGI;
import sgo.mobile.mava.frameworks.math.FormatCurrency;

public class DetailDGIReportAdapter extends BaseAdapter {
    private Activity activity;

    public DetailDGIReportAdapter(Activity act) {
        this.activity = act;
    }
    public int getCount() {
        // TODO Auto-generated method stub
        return FragmentDetailReportDGI.doc_no.size();
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolder holder;

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.fragment_detail_dgi_report_item, null);
            holder = new ViewHolder();

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        Double dbl_amount = Double.parseDouble(FragmentDetailReportDGI.amount.get(position));
        int int_amount = dbl_amount.intValue();

        Double dbl_fee = Double.parseDouble(FragmentDetailReportDGI.fee);
        int int_fee    = dbl_fee.intValue();

        int total_amount = int_amount + int_fee;

        holder.txtText     = (TextView) convertView.findViewById(R.id.txtText);
        holder.txtSubText  = (TextView) convertView.findViewById(R.id.txtSubText);
        holder.txtSubText2 = (TextView) convertView.findViewById(R.id.txtSubText2);

        holder.txtText.setText("Invoice " + FragmentDetailReportDGI.doc_no.get(position));
        holder.txtSubText.setText("Jumlah : " +  FormatCurrency.getRupiahFormat(Integer.toString(int_amount)));
        holder.txtSubText2.setText("Payment Type : " +  FragmentDetailReportDGI.payment_type.get(position));

        return convertView;
    }

    static class ViewHolder {
        TextView txtText, txtSubText, txtSubText2;
    }
}

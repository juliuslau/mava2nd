package sgo.mobile.mava.app.adapter;

import java.util.ArrayList;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import sgo.mobile.mava.R;
import sgo.mobile.mava.app.beans.AmountDGICCLBean;
import sgo.mobile.mava.app.fragments.FragmentRandomCCLDI;
import sgo.mobile.mava.frameworks.math.FormatCurrency;

public class RandomCCLDIAdapter extends BaseAdapter {
    Context ctx;
    LayoutInflater lInflater;
    ArrayList<AmountDGICCLBean> objects;

    public RandomCCLDIAdapter(Context context, ArrayList<AmountDGICCLBean> products) {
        ctx = context;
        objects = products;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            view = lInflater.inflate(R.layout.fragment_random_di_ccl_item, parent, false);
        }else{
        }

        AmountDGICCLBean p = getProduct(position);

        Double dbl_amount = Double.parseDouble(FragmentRandomCCLDI.amount.get(position));
        int int_amount = dbl_amount.intValue();


        ((TextView) view.findViewById(R.id.txtText)).setText(FragmentRandomCCLDI.member_shop.get(position));
        ((TextView) view.findViewById(R.id.txtSubText)).setText("Jumlah : " + FormatCurrency.getRupiahFormat(Integer.toString(int_amount)));

        CheckBox cbBuy = (CheckBox) view.findViewById(R.id.cbBox);
        cbBuy.setOnCheckedChangeListener(myCheckChangList);
        cbBuy.setTag(position);
        cbBuy.setChecked(p.checked);
        return view;
    }

    public AmountDGICCLBean getProduct(int position) {
        return ((AmountDGICCLBean) getItem(position));
    }

    public ArrayList<AmountDGICCLBean> getCheckBox() {
        ArrayList<AmountDGICCLBean> box = new ArrayList<AmountDGICCLBean>();
        for (AmountDGICCLBean p : objects) {
            if (p.checked)
                box.add(p);
        }
        return box;
    }

    OnCheckedChangeListener myCheckChangList = new OnCheckedChangeListener() {
        public void onCheckedChanged(CompoundButton buttonView,
                                     boolean isChecked) {
            getProduct((Integer) buttonView.getTag()).checked = isChecked;
        }
    };
}
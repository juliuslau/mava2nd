package sgo.mobile.mava.app.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import sgo.mobile.mava.R;
import sgo.mobile.mava.app.beans.SpinnerProductBean;

/**
 * Created by thinkpad on 1/21/2016.
 */
public class SpinnerProductAdapter extends ArrayAdapter<SpinnerProductBean> {
    private Activity context;
    ArrayList<SpinnerProductBean> data = null;

    public SpinnerProductAdapter(Activity context, int resource,
                                 ArrayList<SpinnerProductBean> data) {
        super(context, resource, data);
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    private View initView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        if (row == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            row = inflater.inflate(R.layout.simple_spinner_item, parent, false);
        }

        SpinnerProductBean item = data.get(position);

        if (item != null) { // Parse the data from each object and set it.
            TextView name = (TextView) row.findViewById(R.id.item_value);
            if (name != null) {
                name.setText(item.getProduct_name());
            }
        }
        return row;
    }
}

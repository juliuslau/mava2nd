package sgo.mobile.mava.app.beans;

public class AmountDGICCLBean {
    public String hold_id = "";
    public String amount  = "";
    public String member_shop = "";
    public String created = "";
    public boolean checked = false;

    public AmountDGICCLBean(String hold_id, String amount, String member_shop, String created, boolean checked) {
        this.hold_id = hold_id;
        this.amount = amount;
        this.member_shop = member_shop;
        this.created = created;
        this.checked = checked;
    }

    public String getHold_id() {
        return hold_id;
    }

    public void setHold_id(String hold_id) {
        this.hold_id = hold_id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getMember_shop() {
        return member_shop;
    }

    public void setMember_shop(String member_shop) {
        this.member_shop = member_shop;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    @Override
    public String toString() {
        return member_shop;
    }
}

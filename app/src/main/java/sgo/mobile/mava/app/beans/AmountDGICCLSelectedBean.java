package sgo.mobile.mava.app.beans;

public class AmountDGICCLSelectedBean {
    public String hold_id = "";
    public String amount  = "";
    public String member_shop = "";
    public String created = "";

    public AmountDGICCLSelectedBean(String hold_id, String amount, String member_shop, String created) {
        this.hold_id = hold_id;
        this.amount = amount;
        this.member_shop = member_shop;
        this.created = created;
    }

    public String getHold_id() {
        return hold_id;
    }

    public void setHold_id(String hold_id) {
        this.hold_id = hold_id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getMember_shop() {
        return member_shop;
    }

    public void setMember_shop(String member_shop) {
        this.member_shop = member_shop;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    @Override
    public String toString() {
        return member_shop;
    }
}

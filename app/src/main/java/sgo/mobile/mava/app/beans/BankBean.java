package sgo.mobile.mava.app.beans;

public class BankBean {
    public String bank_code = "";
    public String bank_name = "";
    public String ccy_id = "";
    public String product_code = "";
    public String seller_fee = "";
    public String buyer_fee = "";
    public String tx_fee = "";
    public String member_phone = "";
    public String benef_acct_no = "";
    public String benef_acct_name = "";
    public String charges_acct_no = "";
    public String fee_acct_no = "";

    public BankBean(String _bank_code, String _bank_name, String _ccy_id, String _product_code, String _seller_fee, String _buyer_fee, String _tx_fee, String _member_phone, String _benef_acct_no, String _benef_acct_name, String _charges_acct_no, String _fee_acct_no) {
        bank_code = _bank_code;
        bank_name = _bank_name;
        ccy_id = _ccy_id;
        product_code = _product_code;
        seller_fee = _seller_fee;
        buyer_fee = _buyer_fee;
        tx_fee = _tx_fee;
        member_phone = _member_phone;
        benef_acct_no = _benef_acct_no;
        benef_acct_name = _benef_acct_name;
        charges_acct_no = _charges_acct_no;
        fee_acct_no = _fee_acct_no;
    }

    public String getBank_code() {
        return bank_code;
    }

    public String getBank_name() {
        return bank_name;
    }

    public String getCcy_id() {
        return ccy_id;
    }

    public String getProduct_code() {
        return product_code;
    }

    public String getSeller_fee() {
        return seller_fee;
    }

    public String getBuyer_fee() {
        return buyer_fee;
    }

    public String getTx_fee() {
        return tx_fee;
    }

    public String getMember_phone() {
        return member_phone;
    }

    public String getBenef_acct_no() {
        return benef_acct_no;
    }

    public String getBenef_acct_name() {
        return benef_acct_name;
    }

    public String getCharges_acct_no() {
        return charges_acct_no;
    }

    public String getFee_acct_no() {
        return fee_acct_no;
    }

    public String toString()
    {
        return( bank_name  );
    }
}
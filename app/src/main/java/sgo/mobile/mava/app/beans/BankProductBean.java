package sgo.mobile.mava.app.beans;

public class BankProductBean {
    public String bank_code = "";
    public String product_code = "";
    public String product_name = "";

    public BankProductBean( String _bank_code, String _product_code, String _product_name )
    {
        bank_code = _bank_code;
        product_code = _product_code;
        product_name = _product_name;
    }

    public String getBank_code(){
        return bank_code;
    }

    public void setBank_code(String bank_code){
        this.bank_code = bank_code;
    }

    public String getProduct_code(){
        return product_code;
    }

    public void setProduct_code(String product_code){
        this.product_code = product_code;
    }

    public String getProduct_name(){
        return product_name;
    }

    public void setProduct_name(String product_name){
        this.product_name = product_name;
    }

    public String toString()
    {
        return( product_name  );
    }
}
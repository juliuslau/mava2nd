package sgo.mobile.mava.app.beans;

/**
 * Created by thinkpad on 1/19/2016.
 */
public class CommunityModel {
    private String id;
    private String name;
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

package sgo.mobile.mava.app.beans;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.activeandroid.query.Update;
import com.activeandroid.util.SQLiteUtils;

import java.util.List;

/**
 * Created by thinkpad on 1/19/2016.
 */

@Table(name = "InvoiceRepayment")
public class InvoiceRepaymentBean extends Model {
    @Column
    private String tx_id;

    @Column
    private String amount;

    @Column
    private String ccy_id;

    @Column
    private String max_tx_date;

    @Column
    private String desc;

    @Column
    private String selected;

    public InvoiceRepaymentBean() {
        super();
    }

    public InvoiceRepaymentBean(String _tx_id, String _max_tx_date, String _amount, String _ccy_id, String _desc, String _selected) {
        super();
        this.setTx_id(_tx_id);
        this.setMax_tx_date(_max_tx_date);
        this.setAmount(_amount);
        this.setCcy_id(_ccy_id);
        this.setDesc(_desc);
        this.setSelected(_selected);
    }

    public static List<InvoiceRepaymentBean> getAll() {
        // This is how you execute a query
        return new Select()
                .all()
                .from(InvoiceRepaymentBean.class)
                .execute();
    }

    public static void deleteAll() {
        // This is how you execute a query
        new Delete().from(InvoiceRepaymentBean.class).execute();
        SQLiteUtils.execSql("DELETE FROM SQLITE_SEQUENCE WHERE name='InvoiceRepayment';");
    }

    public static void updateSelected(String _selected, String _id){
        new Update(InvoiceRepaymentBean.class).set("selected = ?", _selected).where("tx_id = ?", _id).execute();
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCcy_id() {
        return ccy_id;
    }

    public void setCcy_id(String ccy_id) {
        this.ccy_id = ccy_id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getMax_tx_date() {
        return max_tx_date;
    }

    public void setMax_tx_date(String max_tx_date) {
        this.max_tx_date = max_tx_date;
    }

    public String getSelected() {
        return selected;
    }

    public void setSelected(String selected) {
        this.selected = selected;
    }

    public String getTx_id() {
        return tx_id;
    }

    public void setTx_id(String tx_id) {
        this.tx_id = tx_id;
    }
}

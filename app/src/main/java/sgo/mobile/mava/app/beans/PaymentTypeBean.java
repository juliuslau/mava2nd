package sgo.mobile.mava.app.beans;

public class PaymentTypeBean {
    public String payment_type  = "";
    public String payment_name  = "";

    public PaymentTypeBean(String payment_type, String payment_name) {
        this.payment_type = payment_type;
        this.payment_name = payment_name;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public String getPayment_name() {
        return payment_name;
    }

    public void setPayment_name(String payment_name) {
        this.payment_name = payment_name;
    }

    @Override
    public String toString() {
        return  payment_name;
    }
}

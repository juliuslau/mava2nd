package sgo.mobile.mava.app.beans;/*
  Created by Administrator on 5/19/2015.
 */

public class ReportListEspayModel {

    private String datetime;
    private String ccy_id;
    private String amount;
    private String admin_fee;
    private String total_amount;
    private String remark;
    private String tx_id;
    private String bank_name;
    private String product_name;
    private String tx_status;
    private String member_name;


    public ReportListEspayModel(String _datetime, String _ccy_id, String _amount,
                                String _admin_fee, String _remark, String _tx_id,
                                String _bank_name, String _product_name, String _tx_status,
                                String _member_name, String _total_amount){
        this.setDatetime(_datetime);
        this.setCcy_id(_ccy_id);
        this.setAmount(_amount);
        this.setAdmin_fee(_admin_fee);
        this.setRemark(_remark);
        this.setTx_id(_tx_id);
        this.setBank_name(_bank_name);
        this.setProduct_name(_product_name);
        this.setTx_status(_tx_status);
        this.setMember_name(_member_name);
        this.setTotal_amount(_total_amount);
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public String getMember_name() {
        return member_name;
    }

    public void setMember_name(String member_name) {
        this.member_name = member_name;
    }

    public String getDatetime() {
    return datetime;
    }

    public void setDatetime(String datetime) {
    this.datetime = datetime;
    }

    public String getCcy_id() {
    return ccy_id;
    }

    public void setCcy_id(String ccy_id) {
    this.ccy_id = ccy_id;
    }

    public String getAmount() {
    return amount;
    }

    public void setAmount(String amount) {
    this.amount = amount;
    }

    public String getAdmin_fee() {
    return admin_fee;
    }

    public void setAdmin_fee(String admin_fee) {
    this.admin_fee = admin_fee;
    }

    public String getRemark() {
    return remark;
    }

    public void setRemark(String remark) {
    this.remark = remark;
    }

    public String getTx_id() {
    return tx_id;
    }

    public void setTx_id(String tx_id) {
    this.tx_id = tx_id;
    }

    public String getBank_name() {
    return bank_name;
    }

    public void setBank_name(String bank_name) {
    this.bank_name = bank_name;
    }

    public String getProduct_name() {
    return product_name;
    }

    public void setProduct_name(String product_name) {
    this.product_name = product_name;
    }

    public String getTx_status() {
    return tx_status;
    }

    public void setTx_status(String tx_status) {
    this.tx_status = tx_status;
    }

}

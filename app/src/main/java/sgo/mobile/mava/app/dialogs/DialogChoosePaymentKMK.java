package sgo.mobile.mava.app.dialogs;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import sgo.mobile.mava.R;

/**
 * Created by thinkpad on 2/3/2016.
 */
public class DialogChoosePaymentKMK extends DialogFragment {

    View view;
    RadioGroup radioPaymentGroup;
    RadioButton radioKmkButton, radioSavingButton, radioKumButton;
    Button btnOk;
    String selected;

    public interface NoticeDialogListener {
        void onChoosePaymentKMK(String rbSelected);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialog_choose_payment_kmk, container);
        getDialog().requestWindowFeature(android.view.Window.FEATURE_NO_TITLE);

        radioPaymentGroup = (RadioGroup) view.findViewById(R.id.rg_payment_via);
        radioKmkButton = (RadioButton) view.findViewById(R.id.rb_kmk);
        radioSavingButton = (RadioButton) view.findViewById(R.id.rb_saving_giro);
        radioKumButton = (RadioButton) view.findViewById(R.id.rb_kum);
        btnOk = (Button) view.findViewById(R.id.btn_ok);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedId = radioPaymentGroup.getCheckedRadioButtonId();

                switch(selectedId) {
                    case R.id.rb_kmk:
                        selected = radioKmkButton.getText().toString();
                        break;
                    case R.id.rb_saving_giro:
                        selected =  radioSavingButton.getText().toString();
                        break;
                    case R.id.rb_kum:
                        selected = radioKumButton.getText().toString();
                        break;
                }

                NoticeDialogListener activity = (NoticeDialogListener) getTargetFragment();
                activity.onChoosePaymentKMK(selected);
                dismiss();
            }
        });

        return view;
    }

}

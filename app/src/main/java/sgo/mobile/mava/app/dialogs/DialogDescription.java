package sgo.mobile.mava.app.dialogs;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import sgo.mobile.mava.R;
import sgo.mobile.mava.app.beans.AutoResizeTextView;
import sgo.mobile.mava.conf.AppParams;

/**
 * Created by thinkpad on 7/23/2015.
 */
public class DialogDescription extends DialogFragment {

    View view;
    LinearLayout layout_type, layout_bank, layout_remark, layout_phone;
    TextView desc_type, desc_bank, desc_phone;
    AutoResizeTextView desc_remark;
    Button btnOK;

    String payment_type_code, payment_type_name, payment_bankname, payment_remark, phone_number;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialog_description, container);
        getDialog().requestWindowFeature(android.view.Window.FEATURE_NO_TITLE);

        layout_type = (LinearLayout) view.findViewById(R.id.layout_type);
        layout_bank = (LinearLayout) view.findViewById(R.id.layout_bank);
        layout_remark = (LinearLayout) view.findViewById(R.id.layout_remark);
        layout_phone = (LinearLayout) view.findViewById(R.id.layout_phone);

        desc_type = (TextView) view.findViewById(R.id.desc_type);
        desc_bank = (TextView) view.findViewById(R.id.desc_bank);
        desc_remark = (AutoResizeTextView) view.findViewById(R.id.desc_remark);
        desc_phone = (TextView) view.findViewById(R.id.desc_phone);
        btnOK = (Button) view.findViewById(R.id.btn_ok);

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        //untuk bug AutoResizeTextView Android 3.1 - 4.04
        final String DOUBLE_BYTE_SPACE = "\u3000";
        String fixString = "";
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB_MR1
                && android.os.Build.VERSION.SDK_INT <= android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
            fixString = DOUBLE_BYTE_SPACE;
        }

        Bundle bundle = getArguments();
        if(bundle != null) {
            payment_type_code = bundle.getString("payment_type_code");
            payment_type_name = bundle.getString("payment_type_name");
            payment_remark = bundle.getString("payment_remark");
            payment_bankname = bundle.getString("payment_bankname");
            phone_number = bundle.getString("phone_number");

            layout_type.setVisibility(View.VISIBLE);
            layout_remark.setVisibility(View.VISIBLE);

            desc_type.setText(payment_type_name);
            desc_remark.setText(fixString + payment_remark + fixString);

            if(phone_number.equals("")) {
                layout_phone.setVisibility(View.GONE);
            } else {
                layout_phone.setVisibility(View.VISIBLE);
                desc_phone.setText(phone_number);
            }

            if(payment_type_code.equalsIgnoreCase(AppParams.BG) || payment_type_code.equalsIgnoreCase(AppParams.TS)) {
                layout_bank.setVisibility(View.VISIBLE);
                desc_bank.setText(payment_bankname);
            }
        }

        return view;
    }
}

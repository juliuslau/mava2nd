package sgo.mobile.mava.app.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import sgo.mobile.mava.conf.AplConstants;
import sgo.mobile.mava.conf.AppParams;
import sgo.mobile.mava.frameworks.net.loopj.android.http.MySSLSocketFactory;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.mava.R;
import sgo.mobile.mava.app.activities.MainActivity;
import sgo.mobile.mava.app.ui.dialog.DefinedDialog;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpClient;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpResponseHandler;
import sgo.mobile.mava.frameworks.net.loopj.android.http.RequestParams;

public class FragmentC2CCashOut extends Fragment {
    private ProgressDialog pDialog;
    FragmentManager fm;
    String Result;
    String community_code, requester_id, sender_no, ref_code;

    EditText txtOtp;
    EditText txt_mobile_number;
    Button btnDone;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_c2c_cashout, container, false);

        txtOtp            = (EditText) view.findViewById(R.id.txtOtp);
        txt_mobile_number = (EditText) view.findViewById(R.id.txt_mobile_number);

        btnDone               = (Button) view.findViewById(R.id.btnDone);
        btnDone.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                // TODO Auto-generated method stub


                community_code       = AplConstants.CommCodeCTC;
                requester_id         = AplConstants.MemberCodeCTC;
                sender_no            = txt_mobile_number.getText().toString();
                ref_code             = txtOtp.getText().toString();

                if(sender_no.equalsIgnoreCase("") || ref_code.equalsIgnoreCase(""))
                {
                    Toast.makeText(getActivity(), R.string.form_alert, Toast.LENGTH_SHORT).show();
                }else{

                    pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, "Processing C2C Cash Out...");
                    AsyncHttpClient client = new AsyncHttpClient();
                    client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
                    client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
                    RequestParams params   = new RequestParams();

                    params.put("community_code", community_code);
                    params.put("requester_id", requester_id);
                    params.put("sender_no", sender_no);
                    params.put("ref_code", ref_code);

                    Log.d("params", params.toString());
                    client.post(AplConstants.CashOutC2CMobileAPI, params, new AsyncHttpResponseHandler() {
                        public void onSuccess(String content) {
                            Log.d("result:", content);
                            try {
                                JSONObject object         = new JSONObject(content);

                                String rq_uuid            = object.getString("rq_uuid");
                                String error_code         = object.getString("error_code");
                                String error_msg          = object.getString("error_msg");

                                if (pDialog != null) {
                                    pDialog.dismiss();
                                }

                                if (error_code.equals(AppParams.SUCCESS_CODE)) {
                                    String transaction_id     = object.getString("transaction_id");
                                    String fee_out            = object.getString("fee_out");
                                    String ref                = object.getString("ref");

                                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                    alert.setTitle("Cash To Cash");
                                    alert.setMessage("C2C Cash Out Success");
                                    alert.setPositiveButton("OK", null);
                                    alert.show();

                                } else {
                                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                    alert.setTitle("Cash To Cash");
                                    alert.setMessage("Cash To Cash : " + error_msg);
                                    alert.setPositiveButton("OK", null);
                                    alert.show();
                                }

                            } catch (JSONException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        };

                        public void onFailure(Throwable error, String content) {
                            if (pDialog != null) {
                                pDialog.dismiss();
                            }
                            Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });


        return view;
    }

    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
        }

    };

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
    }
}
package sgo.mobile.mava.app.fragments;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;
import in.srain.cube.views.ptr.header.StoreHouseHeader;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.mava.AppHelper;
import sgo.mobile.mava.R;
import sgo.mobile.mava.app.activities.MainActivity;
import sgo.mobile.mava.app.adapter.CommunityDGIReportAdapter;
import sgo.mobile.mava.app.adapter.DiReportEspayAdapter;
import sgo.mobile.mava.app.adapter.PaymentTypeAdapter;
import sgo.mobile.mava.app.beans.PaymentTypeBean;
import sgo.mobile.mava.app.beans.ReportCommunityBean;
import sgo.mobile.mava.app.beans.ReportListEspayModel;
import sgo.mobile.mava.app.dialogs.ReportBillerDialog;
import sgo.mobile.mava.conf.AplConstants;
import sgo.mobile.mava.conf.AppParams;
import sgo.mobile.mava.conf.InetHandler;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpClient;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpResponseHandler;
import sgo.mobile.mava.frameworks.net.loopj.android.http.MySSLSocketFactory;
import sgo.mobile.mava.frameworks.net.loopj.android.http.RequestParams;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class FragmentCommunityReportDGI extends Fragment implements ReportBillerDialog.OnDialogOkCallback {
    FragmentManager fm;

    // declare view objects
    ListView listMenu;
    ProgressBar prgLoading;
    TextView txtAlert;
    TextView lbl_header;
    Button btnFilterDate;

    CommunityDGIReportAdapter communityDGIAdapter;
    DiReportEspayAdapter espayAdapter;

    // create arraylist variables to store data from server
//    public static ArrayList<String> id       = new ArrayList<String>();
//    public static ArrayList<String> code     = new ArrayList<String>();
//    public static ArrayList<String> name     = new ArrayList<String>();
//    public static ArrayList<String> sales_alias    = new ArrayList<String>();
//    public static ArrayList<String> ccy_id         = new ArrayList<String>();
//    public static ArrayList<String> amount         = new ArrayList<String>();

    ArrayList<ReportCommunityBean> arrReportComm ;
    ArrayList<ReportListEspayModel> arrReportEspay ;

    // create price format
    DecimalFormat formatData = new DecimalFormat("#.##");
    TextView tv_date_from, tv_date_to;
    final String DATEFROM = "tagFrom";
    final String DATETO = "tagTo";
    Calendar date_from, date_to,bak_date_to,bak_date_from;
//    ToggleButton filter_btn;
    Boolean chooseDate = false;

    //pull to refresh
    int page = 0;
    private PtrFrameLayout mPtrFrame;

    //spinner
    Spinner cbo_payment_type;
    String type = "CT";
    private ArrayList<PaymentTypeBean> PaymentTypeList = new ArrayList<PaymentTypeBean>();
    private Activity act;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view  = inflater.inflate(R.layout.fragment_comm_dgi_report, container, false);

        lbl_header = (TextView) view.findViewById(R.id.label_header);
        tv_date_from =  (TextView) view.findViewById(R.id.filter_date_from);
        tv_date_to =  (TextView) view.findViewById(R.id.filter_date_to);
        cbo_payment_type = (Spinner) view.findViewById(R.id.filter_payment_type);
        btnFilterDate = (Button) view.findViewById(R.id.filter_date_btn);
        txtAlert = (TextView) view.findViewById(R.id.txtAlert);
        prgLoading = (ProgressBar) view.findViewById(R.id.prgLoading);
        mPtrFrame = (PtrFrameLayout) view.findViewById(R.id.report_ptr_frame);
        listMenu   = (ListView) view.findViewById(R.id.report_list_comm);

        act = getActivity();

        arrReportComm = new ArrayList<>();
        communityDGIAdapter = new CommunityDGIReportAdapter(getActivity(), arrReportComm);

        arrReportEspay = new ArrayList<>();
        espayAdapter = new DiReportEspayAdapter(getActivity(), arrReportEspay);

        PaymentTypeList.add(new PaymentTypeBean("CT", "Cash Tunai"));
        PaymentTypeList.add(new PaymentTypeBean("BG", "Bilyet Giro"));
        PaymentTypeList.add(new PaymentTypeBean("TS", "Transfer Slip"));
        PaymentTypeList.add(new PaymentTypeBean("ESPAY", "Espay"));

        PaymentTypeAdapter paymentTypeAdapter = new PaymentTypeAdapter(getActivity(), android.R.layout.simple_spinner_item, PaymentTypeList);
        cbo_payment_type.setAdapter(paymentTypeAdapter);

        //set spinner awal
        cbo_payment_type.setSelection(0);
        PaymentTypeBean paymentSelected  = (PaymentTypeBean) cbo_payment_type.getSelectedItem();
        type   = String.valueOf(paymentSelected.payment_type);

        final String from = AplConstants.getCurrentDate(6);
        final String to = AplConstants.getCurrentDate();

        date_from = StringToCal(from);
        date_to = StringToCal(to);
        bak_date_from = (Calendar) date_from.clone();
        bak_date_to = (Calendar) date_to.clone();

        String dedate = getString(R.string.from)+" : "+date_from.get(Calendar.DAY_OF_MONTH)+"-"+(date_from.get(Calendar.MONTH)+1)+"-"+date_from.get(Calendar.YEAR);
        tv_date_from.setText(dedate);
        dedate = getString(R.string.to)+" : "+date_to.get(Calendar.DAY_OF_MONTH)+"-"+(date_to.get(Calendar.MONTH)+1)+"-"+date_to.get(Calendar.YEAR);
        tv_date_to.setText(dedate);

        tv_date_from.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseDate = true;
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        dobPickerSetListener,
                        date_from.get(Calendar.YEAR),
                        date_from.get(Calendar.MONTH),
                        date_from.get(Calendar.DAY_OF_MONTH)
                );

                dpd.show(getActivity().getFragmentManager(), DATEFROM);
            }
        });

        tv_date_to.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseDate = true;
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        dobPickerSetListener,
                        date_to.get(Calendar.YEAR),
                        date_to.get(Calendar.MONTH),
                        date_to.get(Calendar.DAY_OF_MONTH)
                );

                dpd.show(getActivity().getFragmentManager(), DATETO);
            }
        });

        cbo_payment_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(InetHandler.isNetworkAvailable(act)) {
                    page = 0;
                    PaymentTypeBean paymentSelected = (PaymentTypeBean) cbo_payment_type.getSelectedItem();
                    type = String.valueOf(paymentSelected.payment_type);
                    if (type.equalsIgnoreCase("espay")) {
                        if (chooseDate)
                            getReportEspay(CalToString(date_from), CalToString(date_to), true, false);
                        else getReportEspay(from, to, true, false);
                    } else {
                        if (chooseDate)
                            getReportCommunity(CalToString(date_from), CalToString(date_to), type, true, false);
                        else getReportCommunity(from, to, type, true, false);
                    }
                }
                else {
                    prgLoading.setVisibility(View.GONE);
                    lbl_header.setVisibility(View.GONE);
                    mPtrFrame.setVisibility(View.GONE);
                    listMenu.setVisibility(View.GONE);
                    txtAlert.setVisibility(View.GONE);
                    Toast.makeText(act, getString(R.string.no_connection), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnFilterDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(InetHandler.isNetworkAvailable(act)) {
                    page = 0;
                    if (type.equalsIgnoreCase("espay")) {
                        if (chooseDate)
                            getReportEspay(CalToString(date_from), CalToString(date_to), true, false);
                        else getReportEspay(from, to, true, false);
                    } else {
                        if (chooseDate)
                            getReportCommunity(CalToString(date_from), CalToString(date_to), type, true, false);
                        else getReportCommunity(from, to, type, true, false);
                    }
                }
                else {
                    prgLoading.setVisibility(View.GONE);
                    lbl_header.setVisibility(View.GONE);
                    mPtrFrame.setVisibility(View.GONE);
                    listMenu.setVisibility(View.GONE);
                    txtAlert.setVisibility(View.GONE);
                    Toast.makeText(act, getString(R.string.no_connection), Toast.LENGTH_LONG).show();
                }
            }
        });

        // event listener to handle list when clicked
        listMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                    long arg3) {
                if(type.equalsIgnoreCase("espay")) {
                    String userId = AppHelper.getUserPhone(getActivity());
                    showReportDialog(arrReportEspay.get(position).getDatetime(), arrReportEspay.get(position).getTx_id(),
                            userId, arrReportEspay.get(position).getMember_name(),
                            arrReportEspay.get(position).getBank_name(), arrReportEspay.get(position).getProduct_name(),
                            arrReportEspay.get(position).getAmount(), arrReportEspay.get(position).getAdmin_fee(),
                            arrReportEspay.get(position).getTotal_amount(), arrReportEspay.get(position).getTx_status(),
                            arrReportEspay.get(position).getRemark());
                }
                else {
                    Fragment newFragment = new FragmentMemberReportDGI();
                    Bundle args = new Bundle();
                    args.putString(AppParams.COMM_ID, arrReportComm.get(position).getComm_id());
                    args.putString(AppParams.COMM_NAME, arrReportComm.get(position).getComm_name());
                    args.putString(AppParams.COMM_CODE, arrReportComm.get(position).getComm_code());
                    args.putString(AppParams.SALES_ALIAS, arrReportComm.get(position).getSales_alias());
                    args.putString(AppParams.CCY_ID, arrReportComm.get(position).getCcy_id());
                    args.putString(AppParams.AMOUNT, arrReportComm.get(position).getAmount());
                    if (chooseDate) {
                        args.putString(AppParams.DATE_FROM, CalToString(date_from));
                        args.putString(AppParams.DATE_TO, CalToString(date_to));
                    } else {
                        args.putString(AppParams.DATE_FROM, from);
                        args.putString(AppParams.DATE_TO, to);
                    }
                    args.putString(AppParams.PAYMENT_TYPE, type);

                    newFragment.setArguments(args);
                    getActivity().getSupportFragmentManager().popBackStack();
                    switchFragment(newFragment);
                }
            }
        });

        StoreHouseHeader header = new StoreHouseHeader(getActivity().getApplicationContext());
        header.setPadding(0, 20, 0, 20);
        header.setTextColor(Color.BLACK);
        header.initWithString("Updating...");

        mPtrFrame.setDurationToCloseHeader(1500);
        mPtrFrame.setHeaderView(header);
        mPtrFrame.addPtrUIHandler(header);
        mPtrFrame.setPtrHandler(new PtrHandler() {
            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                frame.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(InetHandler.isNetworkAvailable(act)) {
                            if (txtAlert.getVisibility() == View.VISIBLE) {

                            } else {
                                page++;
                                if (type.equalsIgnoreCase("espay")) {
                                    if (chooseDate)
                                        getReportEspay(CalToString(date_from), CalToString(date_to), false, true);
                                    else getReportEspay(from, to, false, true);
                                } else {
                                    if (chooseDate)
                                        getReportCommunity(CalToString(date_from), CalToString(date_to), type, false, true);
                                    else getReportCommunity(from, to, type, false, true);
                                }
                            }
                        }
                        else {
                            Toast.makeText(act, getString(R.string.no_connection), Toast.LENGTH_LONG).show();
                        }
                        mPtrFrame.refreshComplete();
                    }
                }, 1800);
            }

            @Override
            public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
                return !canScrollUp( content); // or cast with ListView
            }

            public boolean canScrollUp(View view) {
                if (android.os.Build.VERSION.SDK_INT < 14) {
                    if (view instanceof AbsListView) {
                        final AbsListView absListView = (AbsListView) view;
                        return absListView.getChildCount() > 0
                                && (absListView.getFirstVisiblePosition() > 0 || absListView
                                .getChildAt(0).getTop() < absListView.getPaddingTop());
                    } else {
                        return view.getScrollY() > 0;
                    }
                } else {
                    return ViewCompat.canScrollVertically(view, -1);
                }
            }
        });

        return view;
    }

    public void getReportCommunity(String dateFrom, String dateTo, String type, boolean newArr, final boolean refresh){
        if (newArr) arrReportComm.clear();
        if (!refresh) {
            txtAlert.setVisibility(View.GONE);
            prgLoading.setVisibility(View.VISIBLE);
            lbl_header.setVisibility(View.GONE);
            mPtrFrame.setVisibility(View.GONE);
            listMenu.setVisibility(View.GONE);
        }
        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
        client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
        RequestParams params = new RequestParams();

        String param_sales_id = AppHelper.getUserId(getActivity());
        params.put(AppParams.SALES_ID, param_sales_id);
        params.put(AppParams.PAGE, Integer.toString(page));
        params.put(AppParams.DATE_FROM, dateFrom);
        params.put(AppParams.DATE_TO, dateTo);
        params.put(AppParams.PAYMENT_TYPE, type);

        Log.d("params", params.toString());
        client.post(AplConstants.DiReportCommunity, params, new AsyncHttpResponseHandler() {
            public void onSuccess(String content) {
                Log.d("result:", content);
                try {
                    // parse json data and store into arraylist variables
                    JSONObject json = new JSONObject(content);
                    String error_code = json.getString("error_code");
                    String error_message = json.getString("error_message");

                    if (error_code.equals(AppParams.SUCCESS_CODE)) {
                        JSONArray data = json.getJSONArray("report_data"); // this is the "items: [ ] part

                        for (int i = 0; i < data.length(); i++) {
                            String comm_id = data.getJSONObject(i).getString(AppParams.COMM_ID);
                            boolean flagSame = false;

                            // cek apakah ada comm id yang sama.. kalau ada, tidak dimasukan ke array
                            if (arrReportComm.size() > 0) {
                                for (int index = 0; index < arrReportComm.size(); index++) {
                                    if (arrReportComm.get(index).getComm_id().equalsIgnoreCase(comm_id)) {
                                        flagSame = true;
                                        break;
                                    } else {
                                        flagSame = false;
                                    }
                                }
                            }

                            if (flagSame == false) {
                                String comm_code = data.getJSONObject(i).getString(AppParams.COMM_CODE);
                                String comm_name = data.getJSONObject(i).getString(AppParams.COMM_NAME);
                                String sales_alias = data.getJSONObject(i).getString(AppParams.SALES_ALIAS);
                                String ccy_id = data.getJSONObject(i).getString(AppParams.CCY_ID);
                                String amount = data.getJSONObject(i).getString(AppParams.AMOUNT);

                                ReportCommunityBean reportComm = new ReportCommunityBean();
                                reportComm.setComm_id(comm_id);
                                reportComm.setComm_code(comm_code);
                                reportComm.setComm_name(comm_name);
                                reportComm.setSales_alias(sales_alias);
                                reportComm.setCcy_id(ccy_id);
                                reportComm.setAmount(amount);

                                arrReportComm.add(reportComm);
                            }
                        }

                        communityDGIAdapter.notifyDataSetChanged();

                        // if data available show data on list
                        if (arrReportComm.size() > 0) {
                            txtAlert.setVisibility(View.GONE);
                            lbl_header.setVisibility(View.VISIBLE);
                            mPtrFrame.setVisibility(View.VISIBLE);
                            listMenu.setVisibility(View.VISIBLE);
                            listMenu.setAdapter(communityDGIAdapter);
                        }
                        // when finish parsing, hide progressbar
                        if (!refresh) prgLoading.setVisibility(View.GONE);
                    } else if (arrReportComm.size() == 0) {
                        if (!refresh) prgLoading.setVisibility(View.GONE);
                        txtAlert.setVisibility(View.VISIBLE);
                        txtAlert.setText(error_message);
                        lbl_header.setVisibility(View.GONE);
                        mPtrFrame.setVisibility(View.GONE);
                        listMenu.setVisibility(View.GONE);
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    if (!refresh) prgLoading.setVisibility(View.GONE);
                    txtAlert.setVisibility(View.VISIBLE);
                    txtAlert.setText(getString(R.string.data_mistake));
                }
            }

            public void onFailure(Throwable error, String content) {
                if (!refresh) prgLoading.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
            }

        });
    }

    public void getReportEspay(String dateFrom, String dateTo, boolean newArr, final boolean refresh){
        if (newArr) arrReportEspay.clear();
        if (!refresh) {
            txtAlert.setVisibility(View.GONE);
            prgLoading.setVisibility(View.VISIBLE);
            lbl_header.setVisibility(View.GONE);
            mPtrFrame.setVisibility(View.GONE);
            listMenu.setVisibility(View.GONE);
        }
        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
        client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
        RequestParams params = new RequestParams();

        String param_sales_id = AppHelper.getUserId(getActivity());
        params.put(AppParams.SALES_ID, param_sales_id);
        params.put(AppParams.PAGE, Integer.toString(page));
        params.put(AppParams.DATE_FROM, dateFrom);
        params.put(AppParams.DATE_TO, dateTo);

        Log.d("params report espay", params.toString());
        client.post(AplConstants.DiReportEspay, params, new AsyncHttpResponseHandler() {
            public void onSuccess(String content) {
                Log.d("result report espay", content);
                try {
                    // parse json data and store into arraylist variables
                    JSONObject json = new JSONObject(content);
                    String error_code = json.getString("error_code");
                    String error_message = json.getString("error_message");

                    if (error_code.equals(AppParams.SUCCESS_CODE)) {
                        JSONArray data = json.getJSONArray("report_data"); // this is the "items: [ ] part

                        for (int i = 0; i < data.length(); i++) {
                            String tx_id = data.getJSONObject(i).getString(AppParams.TX_ID);
                            boolean flagSame = false;

                            // cek apakah ada comm id yang sama.. kalau ada, tidak dimasukan ke array
                            if (arrReportEspay.size() > 0) {
                                for (int index = 0; index < arrReportEspay.size(); index++) {
                                    if (arrReportEspay.get(index).getTx_id().equalsIgnoreCase(tx_id)) {
                                        flagSame = true;
                                        break;
                                    } else {
                                        flagSame = false;
                                    }
                                }
                            }

                            if (flagSame == false) {

                                ReportListEspayModel reportEspay = new ReportListEspayModel(data.getJSONObject(i).getString(AppParams.CREATED),
                                        data.getJSONObject(i).getString(AppParams.CCY_ID), data.getJSONObject(i).getString(AppParams.AMOUNT),
                                        data.getJSONObject(i).getString(AppParams.ADMINFEE), data.getJSONObject(i).getString(AppParams.TX_REASON),
                                        data.getJSONObject(i).getString(AppParams.TX_ID), data.getJSONObject(i).getString(AppParams.BANK_NAME),
                                        data.getJSONObject(i).getString(AppParams.PRODUCT_NAME), data.getJSONObject(i).getString(AppParams.TX_STATUS),
                                        data.getJSONObject(i).getString(AppParams.MEMBER_NAME), data.getJSONObject(i).getString(AppParams.TOTALAMOUNT));

                                arrReportEspay.add(reportEspay);
                            }
                        }

                        espayAdapter.notifyDataSetChanged();

                        // if data available show data on list
                        if (arrReportEspay.size() > 0) {
                            txtAlert.setVisibility(View.GONE);
                            lbl_header.setVisibility(View.GONE);
                            mPtrFrame.setVisibility(View.VISIBLE);
                            listMenu.setVisibility(View.VISIBLE);
                            listMenu.setAdapter(espayAdapter);
                        }
                        // when finish parsing, hide progressbar
                        if (!refresh) prgLoading.setVisibility(View.GONE);
                    } else if (arrReportEspay.size() == 0) {
                        if (!refresh) prgLoading.setVisibility(View.GONE);
                        txtAlert.setVisibility(View.VISIBLE);
                        txtAlert.setText(error_message);
                        lbl_header.setVisibility(View.GONE);
                        mPtrFrame.setVisibility(View.GONE);
                        listMenu.setVisibility(View.GONE);
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    if (!refresh) prgLoading.setVisibility(View.GONE);
                    txtAlert.setVisibility(View.VISIBLE);
                    txtAlert.setText(getString(R.string.data_mistake));
                }
            }

            public void onFailure(Throwable error, String content) {
                if (!refresh) prgLoading.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
            }

        });
    }

    private void showReportDialog(String date, String txId, String custID, String custName, String bankName,String productName,
                                  String amount, String fee, String total, String txStatus, String txRemark) {
        Bundle args = new Bundle();
        ReportBillerDialog dialog = ReportBillerDialog.newInstance(getActivity(), false);
        args.putString(AppParams.DATE_TIME,date);
        args.putString(AppParams.TX_ID, txId);
        args.putString(AppParams.MEMBER_CUST_ID, custID);
        args.putString(AppParams.MEMBER_CUST_NAME, custName);
        args.putString(AppParams.BANK_NAME, bankName);
        args.putString(AppParams.PRODUCT_NAME, productName);
        args.putString(AppParams.AMOUNT, amount);
        args.putString(AppParams.ADMIN_FEE, fee);
        args.putString(AppParams.TOTAL_PAY, total);
        args.putString(AppParams.TX_STATUS, txStatus);
        args.putString(AppParams.TX_REMARK, txRemark);
        dialog.setArguments(args);
        dialog.setTargetFragment(this, 0);
        dialog.show(getActivity().getSupportFragmentManager(), ReportBillerDialog.TAG);
    }

    private Calendar StringToCal(String src){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd",new Locale("id","INDONESIA"));
        Calendar tempCalendar = Calendar.getInstance();

        try {
            tempCalendar.setTime(format.parse(src));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return tempCalendar;
    }

    private String CalToString(Calendar src){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd",new Locale("id","INDONESIA"));
        return format.format(src.getTime());
    }

    DatePickerDialog.OnDateSetListener dobPickerSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
            String dedate;

            if(view.getTag().equals(DATEFROM)){
                dedate = getString(R.string.from)+" : "+dayOfMonth+"-"+(monthOfYear+1)+"-"+year;
                bak_date_from = (Calendar) date_from.clone();
                date_from.set(year,monthOfYear,dayOfMonth);
                tv_date_from.setText(dedate);
            }
            else {
                dedate = getString(R.string.to)+" : "+dayOfMonth+"-"+(monthOfYear+1)+"-"+year;
                bak_date_to = (Calendar) date_to.clone();
                date_to.set(year, monthOfYear, dayOfMonth);
                tv_date_to.setText(dedate);
            }
        }
    };

    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
        }

    };

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
    }

    @Override
    public void onOkButton() {

    }
}
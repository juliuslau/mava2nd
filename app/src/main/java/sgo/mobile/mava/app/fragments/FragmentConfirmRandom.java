package sgo.mobile.mava.app.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import sgo.mobile.mava.conf.AplConstants;
import sgo.mobile.mava.conf.AppParams;
import sgo.mobile.mava.frameworks.net.loopj.android.http.MySSLSocketFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.mava.R;
import sgo.mobile.mava.app.activities.MainActivity;
import sgo.mobile.mava.app.adapter.ConfirmRandomAdapter;
import sgo.mobile.mava.app.ui.dialog.DefinedDialog;
import sgo.mobile.mava.frameworks.math.FormatCurrency;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpClient;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpResponseHandler;
import sgo.mobile.mava.frameworks.net.loopj.android.http.RequestParams;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class FragmentConfirmRandom extends Fragment {
    private ProgressDialog pDialog;
    FragmentManager fm;

    public String transfer_code;
    public String tx_remark, bank_code, bank_name, product_code, ccy_id, no_phone_source, benef_acct_no, benef_acct_name, tx_fee, fee_acct_no, charges_acct_no, buyer_fee, seller_fee, commission_fee, commission_acct_no, bbs_product, resend_token;
    public String comm_id, comm_name, comm_code, buss_scheme_code;
    public String member_id, member_code;
    private String source_acct_no, source_acct_name, payment_id, payment_amount, payment_ref_tx, payment_ref_buyer, payment_ref_seller, payment_ref_commission, payment_status, payment_remark, payment_created;
    private String flag_continue = "Y";
    private String Rq_token_data;
    private String sales_id, sales_alias, sales_name, flag_invoice, bank_data, hold_amount, amount_data_val, tx_fee_val, buyer_fee_val, seller_fee_val, commission_fee_val;
    public String total_invoice;
    public String invoice_data, invoice_data_ori, invoice_data_selected;

    // declare view objects
    ListView listMenu;
    ProgressBar prgLoading;
    TextView txtAlert;
    TextView lbl_header;
    TableLayout tabel_footer;

    ConfirmRandomAdapter confirmRandomAdapter;

    // create arraylist variables to store data from server
    public static ArrayList<String> hold_id = new ArrayList<String>();
    public static ArrayList<String> amount = new ArrayList<String>();
    public static ArrayList<String> member_shop = new ArrayList<String>();
    public static ArrayList<String> created = new ArrayList<String>();

    EditText txtOtp;
    Button btnDone;
    Button btnCancel;

    // create price format
    DecimalFormat formatData = new DecimalFormat("#.##");

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view  = inflater.inflate(R.layout.fragment_random_confirm, container, false);
        prgLoading = (ProgressBar) view.findViewById(R.id.prgLoading);
        listMenu   = (ListView) view.findViewById(R.id.listMenu);
        listMenu   = (ListView) view.findViewById(R.id.listMenu);
        tabel_footer = (TableLayout) view.findViewById(R.id.tabel_footer);

        txtOtp                = (EditText) view.findViewById(R.id.txtOtp);
        btnDone               = (Button) view.findViewById(R.id.btnDone);
        btnCancel             = (Button) view.findViewById(R.id.btnCancel);

        Bundle bundle         = this.getArguments();
        comm_id               = bundle.getString("comm_id");
        comm_name             = bundle.getString("comm_name");
        comm_code             = bundle.getString("comm_code");
        buss_scheme_code      = bundle.getString("buss_scheme_code");
        member_code           = bundle.getString("member_code");
        member_id             = bundle.getString("member_id");
        bank_code             = bundle.getString("bank_code");
        bank_name             = bundle.getString("bank_name");
        product_code          = bundle.getString("product_code");
        ccy_id                = bundle.getString("ccy_id");
        tx_remark             = bundle.getString("tx_remark");
        no_phone_source       = bundle.getString("no_phone_source");
        benef_acct_no         = bundle.getString("benef_acct_no");
        benef_acct_name       = bundle.getString("benef_acct_name");
        fee_acct_no           = bundle.getString("fee_acct_no");
        charges_acct_no       = bundle.getString("charges_acct_no");
        buyer_fee             = bundle.getString("buyer_fee");
        buyer_fee             = ((buyer_fee != null) && !buyer_fee.isEmpty()) ? buyer_fee : "0";
        seller_fee            = bundle.getString("seller_fee");
        tx_fee                = bundle.getString("tx_fee");
        commission_fee        = bundle.getString("commission_fee");
        commission_fee        = ((commission_fee != null) && !commission_fee.isEmpty()) ? commission_fee : "0";
        commission_acct_no    = bundle.getString("commission_acct_no");
        resend_token          = bundle.getString("resend_token");

        bbs_product           = bundle.getString("bbs_product_json");
        invoice_data          = bundle.getString("invoice_data_json");
        invoice_data_ori      = bundle.getString("invoice_data");
        invoice_data_selected = bundle.getString("invoice_data_selected");

        sales_id             = bundle.getString("sales_id");
        sales_alias          = bundle.getString("sales_alias");
        sales_name           = bundle.getString("sales_name");
        flag_invoice         = bundle.getString("flag_invoice");
        bank_data            = bundle.getString("bank_data");
        hold_amount          = bundle.getString("hold_amount");

        amount_data_val      = bundle.getString("amount_data_val");
        tx_fee_val           = bundle.getString("tx_fee_val");
        buyer_fee_val        = bundle.getString("buyer_fee_val");
        buyer_fee_val        = ((buyer_fee_val != null) && !buyer_fee_val.isEmpty()) ? buyer_fee_val : "0";
        seller_fee_val       = bundle.getString("seller_fee_val");
        commission_fee_val   = bundle.getString("commission_fee_val");
        commission_fee_val   = ((commission_fee_val != null) && !commission_fee_val.isEmpty()) ? commission_fee_val : "0";
        total_invoice        = bundle.getString("total_invoice");

        Rq_token_data        = bundle.getString("Rq_token_data");

        Double dbl_amount = Double.parseDouble(amount_data_val) + Double.parseDouble(tx_fee_val);
        int int_amount = dbl_amount.intValue();
        TextView lbl_total_invoice = (TextView) view.findViewById(R.id.lbl_total_invoice);
        lbl_total_invoice.setText(FormatCurrency.getRupiahFormat(Integer.toString(int_amount)));

        lbl_header = (TextView) view.findViewById(R.id.label_header);
        lbl_header.setText(sales_name);

        TextView lbl_bank = (TextView) view.findViewById(R.id.lbl_bank_name);
        lbl_bank.setText(bank_name);

        confirmRandomAdapter = new ConfirmRandomAdapter(getActivity());
        initViews();

        btnDone.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                transfer_code        = txtOtp.getText().toString();
                if(transfer_code.equalsIgnoreCase(""))
                {
                    Toast.makeText(getActivity(), R.string.form_alert, Toast.LENGTH_SHORT).show();
                }else{

                    pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, "Memproses Token Payment Point...");
                    AsyncHttpClient client = new AsyncHttpClient();
                    client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
                    client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
                    RequestParams params   = new RequestParams();

                    params.put("data_inv", Rq_token_data);
                    params.put("token_id", transfer_code);
                    params.put("flag_reversal", flag_continue);

                    Log.d("params", params.toString());
                    client.post(AplConstants.ConfirmTokenCCLDIRandomMobileAPI, params, new AsyncHttpResponseHandler() {
                        public void onSuccess(String content) {
                            Log.d("result:", content);
                            try {
                                JSONObject object         = new JSONObject(content);

                                String error_code         = object.getString("error_code");
                                String error_msg          = object.getString("error_message");

                                if (pDialog != null) {
                                    pDialog.dismiss();
                                }

                                if (error_code.equals(AppParams.SUCCESS_CODE)) {

                                    source_acct_no   = object.getString("source_acct_no");
                                    source_acct_name = object.getString("source_acct_name");

                                    JSONArray arrDataConfirmToken = object.getJSONArray("data");

                                    Log.d("params", arrDataConfirmToken.toString());
                                    if(arrDataConfirmToken.length() > 0){
                                        payment_id               = arrDataConfirmToken.getJSONObject(0).getString("trx_id");
                                        payment_amount           = arrDataConfirmToken.getJSONObject(0).getString("trx_amount");
                                        payment_ref_tx           = arrDataConfirmToken.getJSONObject(0).getString("external_trx");
                                        payment_ref_buyer        = arrDataConfirmToken.getJSONObject(0).getString("external_buyer");
                                        payment_ref_seller       = arrDataConfirmToken.getJSONObject(0).getString("external_seller");
                                        payment_ref_commission   = arrDataConfirmToken.getJSONObject(0).getString("external_commission");
                                        payment_status           = arrDataConfirmToken.getJSONObject(0).getString("trx_status");
                                        payment_remark           = arrDataConfirmToken.getJSONObject(0).getString("trx_reason");
                                        payment_created          = arrDataConfirmToken.getJSONObject(0).getString("trx_date");
                                    }else{
                                        payment_id               = "";
                                        payment_amount           = "";
                                        payment_ref_tx           = "";
                                        payment_ref_buyer        = "";
                                        payment_ref_seller       = "";
                                        payment_ref_commission   = "";
                                        payment_status           = "";
                                        payment_remark           = "";
                                        payment_created          = "";
                                    }

                                    if(payment_status.equalsIgnoreCase("S")) {

                                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                        alert.setTitle("Payment Point");
                                        alert.setMessage("Proses Konfirmasi Token Payment Point Berhasil");
                                        alert.setPositiveButton("OK", null);
                                        alert.show();

                                        AsyncHttpClient client = new AsyncHttpClient();
                                        client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
                                        client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
                                        RequestParams params   = new RequestParams();

                                        params.put("member_id", member_id);
                                        params.put("sales_id", sales_id);
                                        params.put("source_acct_no", source_acct_no);
                                        params.put("source_acct_name", source_acct_name);

                                        params.put("bank_code", bank_code);
                                        params.put("product_code", product_code);
                                        params.put("ccy_id", ccy_id);

                                        params.put("seller_fee", seller_fee_val);
                                        params.put("buyer_fee", buyer_fee_val);
                                        params.put("tx_fee", tx_fee_val);
                                        params.put("commission_fee", commission_fee_val);

                                        params.put("benef_acct_no", benef_acct_no);
                                        params.put("benef_acct_name", benef_acct_name);
                                        params.put("charges_acct_no", charges_acct_no);
                                        params.put("fee_acct_no", fee_acct_no);
                                        params.put("commission_acct_no", commission_acct_no);

                                        params.put("payment_id", payment_id);
                                        params.put("amount", payment_amount);
                                        params.put("payment_ref_tx", payment_ref_tx);
                                        params.put("payment_ref_buyer", payment_ref_buyer);
                                        params.put("payment_ref_seller", payment_ref_seller);
                                        params.put("payment_ref_commission", payment_ref_commission);
                                        params.put("payment_status", payment_status);
                                        params.put("payment_remark", tx_remark);
                                        params.put("payment_created", payment_created);

                                        try{
                                            JSONArray mJSONArray1  = new JSONArray(invoice_data);
                                            params.put("invoice", mJSONArray1.toString());
                                        } catch (JSONException e) {
                                            // TODO Auto-generated catch block
                                            e.printStackTrace();
                                            params.put("invoice", "");
                                        }

                                        try{
                                            JSONArray mJSONArray2  = new JSONArray(bbs_product);
                                            params.put("bbs_product", mJSONArray2.toString());
                                        } catch (JSONException e) {
                                            // TODO Auto-generated catch block
                                            e.printStackTrace();
                                            params.put("bbs_product", "");
                                        }

                                        client.post(AplConstants.PaymentReportCCLDIMobileAPI, params, new AsyncHttpResponseHandler() {
                                            public void onSuccess(String content) {
                                                Log.d("result payment report :", content);
                                                try {
                                                    JSONObject object         = new JSONObject(content);

                                                    String error_code         = object.getString("error_code");
                                                    String error_msg          = object.getString("error_message");

                                                    if (error_code.equals(AppParams.SUCCESS_CODE)) {

                                                        Fragment newFragment = null;
                                                        newFragment = new FragmentInputSalesAlias();
                                                        Bundle args = new Bundle();
                                                        args.putString("comm_id", comm_id);
                                                        args.putString("comm_name", comm_name);
                                                        args.putString("comm_code", comm_code);
                                                        args.putString("buss_scheme_code",buss_scheme_code);
                                                        args.putString("member_id", member_id);
                                                        args.putString("member_code", member_code);
                                                        newFragment.setArguments(args);
                                                        switchFragment(newFragment);

                                                    }else{
                                                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                                        alert.setTitle("Payment Point");
                                                        alert.setMessage("Payment Point : " + error_msg);
                                                        alert.setPositiveButton("OK", null);
                                                        alert.show();
                                                    }

                                                } catch (JSONException e) {
                                                    // TODO Auto-generated catch block
                                                    e.printStackTrace();
                                                }
                                            };

                                            public void onFailure(Throwable error, String content) {
                                                if (pDialog != null) {
                                                    pDialog.dismiss();
                                                }
                                                Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                                            }
                                        });
                                    }else {
                                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                        alert.setTitle("Payment Point");
                                        alert.setMessage("Payment Point : " + payment_remark);
                                        alert.setPositiveButton("OK", null);
                                        alert.show();
                                    }

                                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                    alert.setTitle("Payment Point");
                                    alert.setMessage("Payment Point : " + error_msg);
                                    alert.setPositiveButton("OK", null);
                                    alert.show();

                                } else {
                                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                    alert.setTitle("Payment Point");
                                    alert.setMessage("Payment Point : " + error_msg);
                                    alert.setPositiveButton("OK", null);
                                    alert.show();
                                }

                            } catch (JSONException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        };

                        public void onFailure(Throwable error, String content) {
                            if (pDialog != null) {
                                pDialog.dismiss();
                            }
                            Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Fragment newFragment = null;
                newFragment = new FragmentRandomCCLDI();
                Bundle args = new Bundle();

                args.putString("comm_id", comm_id);
                args.putString("comm_name", comm_name);
                args.putString("comm_code", comm_code);
                args.putString("buss_scheme_code", buss_scheme_code);
                args.putString("member_code", member_code);
                args.putString("member_id", member_id);

                args.putString("sales_id", sales_id);
                args.putString("sales_name", sales_name);
                args.putString("sales_alias", sales_alias);
                args.putString("flag_invoice", flag_invoice);

                args.putString("hold_amount", hold_amount);
                args.putString("ccy_id", ccy_id);
                args.putString("bank_data", bank_data);
                args.putString("invoice_data", invoice_data_ori);

                args.putString("total_invoice", total_invoice);

                newFragment.setArguments(args);
                switchFragment(newFragment);
            }
        });

        return view;
    }


    // clear arraylist variables before used
    void clearData(){
        hold_id.clear();
        amount.clear();
        member_shop.clear();
        created.clear();
    }

    private void initViews() {
        try {
            clearData();
            JSONArray data  = new JSONArray(invoice_data_selected);
            int dataCount   = data.length();
            for (int i = 0; i < dataCount; i++) {
                JSONObject object = data.getJSONObject(i);
                hold_id.add(object.getString("hold_id"));
                amount.add(object.getString("amount"));
                member_shop.add(object.getString("member_shop"));
                created.add(object.getString("created"));
            }

            // when finish parsing, hide progressbar
            prgLoading.setVisibility(8);
            // if data available show data on list
            // otherwise, show alert text
            if(hold_id.size() > 0){
                listMenu.setVisibility(0);
                listMenu.setAdapter(confirmRandomAdapter);
                lbl_header.setVisibility(0);
                tabel_footer.setVisibility(0);
            }else{
                txtAlert.setVisibility(0);
            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
        }

    };

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
    }

}
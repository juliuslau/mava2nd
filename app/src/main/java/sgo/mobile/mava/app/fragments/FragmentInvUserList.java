package sgo.mobile.mava.app.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.securepreferences.SecurePreferences;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.mava.AppHelper;
import sgo.mobile.mava.R;
import sgo.mobile.mava.app.activities.MainActivity;
import sgo.mobile.mava.app.adapter.InvUserAdapter;
import sgo.mobile.mava.app.adapter.PaymentTypeAdapter;
import sgo.mobile.mava.app.beans.PaymentTypeBean;
import sgo.mobile.mava.app.beans.SelectAgainSpinner;
import sgo.mobile.mava.app.dialogs.DialogFragmentDGI;
import sgo.mobile.mava.app.dialogs.SMSDialog;
import sgo.mobile.mava.conf.AplConstants;
import sgo.mobile.mava.conf.AppParams;
import sgo.mobile.mava.conf.ReqPermissionClass;
import sgo.mobile.mava.conf.SMSclass;
import sgo.mobile.mava.frameworks.math.FormatCurrency;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpClient;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpResponseHandler;
import sgo.mobile.mava.frameworks.net.loopj.android.http.MySSLSocketFactory;
import sgo.mobile.mava.frameworks.net.loopj.android.http.RequestParams;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class FragmentInvUserList extends Fragment implements DialogFragmentDGI.NoticeDialogListener{
    public static IntentFilter simStateIntentFilter = new IntentFilter("android.intent.action.SIM_STATE_CHANGED");

    private ProgressDialog pDialog;
    Activity activity;
    SecurePreferences sp;
    FragmentManager fm;

    private ArrayList<PaymentTypeBean> PaymentTypeList = new ArrayList<PaymentTypeBean>();
    private static SelectAgainSpinner cbo_payment_type      = null;
    public String payment_type_code = "",
            payment_type_name = null,
            payment_bankname = null,
            payment_bankcode = null,
            payment_productcode = null,
            payment_productname = null,
            payment_product_h2h = null;

    DialogFragmentDGI dialog;
    boolean alertShow = false;

    public String member_id, member_code, member_name, session_id_param;
    public String total_pay;
    public String session_id_var;
    public String comm_id, comm_name, comm_code, buss_scheme_code;
    public String ccy_id = "IDR";
    public String product_values;

    // declare view objects
    ListView listMenu;
    ProgressBar prgLoading;
    TextView txtAlert;
    TextView lbl_header;
    TableLayout tabel_footer;
    TextView lbl_total_pay_amount;

    InvUserAdapter invoicePaymentAdapter;

    // create arraylist variables to store data from server
    private String partial_payment = "";

    public static ArrayList<String> doc_no = new ArrayList<String>();
    public static ArrayList<String> doc_id = new ArrayList<String>();
    public static ArrayList<String> amount = new ArrayList<String>();
    public static ArrayList<String> remain_amount = new ArrayList<String>();
    public static ArrayList<String> input_amount = new ArrayList<String>();
    public static ArrayList<String> hold_amount = new ArrayList<String>();
    public static ArrayList<String> ccy = new ArrayList<String>();
    public static ArrayList<String> doc_desc = new ArrayList<String>();
    public static ArrayList<String> session_id = new ArrayList<String>();

    Button btnDone;
    Button btnCancel;
    Button btnBack;

    //check sim
    boolean isSMSBanking = false;
    private SMSclass smSclass;
    private ReqPermissionClass reqPermissionClass;
    private SMSDialog smsDialog;

    // create price format
    DecimalFormat formatData = new DecimalFormat("#.##");

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view  = inflater.inflate(R.layout.fragment_invpayment_list, container, false);
        activity = getActivity();
        sp = new SecurePreferences(activity);

        txtAlert = (TextView) view.findViewById(R.id.txtAlert);
        prgLoading = (ProgressBar) view.findViewById(R.id.prgLoading);
        listMenu   = (ListView) view.findViewById(R.id.listMenu);
        tabel_footer = (TableLayout) view.findViewById(R.id.tabel_footer);
        lbl_total_pay_amount = (TextView) view.findViewById(R.id.lbl_total_pay_amount);

        btnDone               = (Button) view.findViewById(R.id.btnDone);
        btnCancel             = (Button) view.findViewById(R.id.btnCancel);
        btnBack               = (Button) view.findViewById(R.id.btnBack);

        cbo_payment_type      = (SelectAgainSpinner)view.findViewById(R.id.cbo_payment_type);

        Bundle bundle         = this.getArguments();
        member_id             = bundle.getString("member_id");
        member_code           = bundle.getString("member_code");
        member_name           = bundle.getString("member_name");
        session_id_param      = bundle.getString("session_id_param");
        session_id_param      = (session_id_param.equalsIgnoreCase("null")) ? "" : session_id_param;

        comm_id               = bundle.getString("comm_id");
        comm_name             = bundle.getString("comm_name");
        comm_code             = bundle.getString("comm_code");
        buss_scheme_code      = bundle.getString("buss_scheme_code");


        lbl_header = (TextView) view.findViewById(R.id.label_header);
        lbl_header.setText(member_code);

        invoicePaymentAdapter = new InvUserAdapter(getActivity());
        parseJSONData();

        // event listener to handle list when clicked
        listMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                    long arg3) {
                Fragment newFragment = null;
                newFragment = new FragmentInvUserPayment();
                Bundle args = new Bundle();
                args.putString("doc_no", doc_no.get(position));
                args.putString("doc_id", doc_id.get(position));
                args.putString("amount", amount.get(position));
                args.putString("remain_amount", remain_amount.get(position));
                args.putString("input_amount", input_amount.get(position));
                args.putString("hold_amount", hold_amount.get(position));
                args.putString("ccy", ccy.get(position));
                args.putString("doc_desc", doc_desc.get(position));
                args.putString("partial_payment", partial_payment);
                args.putString("session_id", session_id.get(position));
                args.putString("member_id", member_id);
                args.putString("member_code", member_code);
                args.putString("member_name", member_name);

                args.putString("comm_id", comm_id);
                args.putString("comm_name", comm_name);
                args.putString("comm_code", comm_code);
                args.putString("buss_scheme_code", buss_scheme_code);

                newFragment.setArguments(args);
                switchFragment(newFragment);
            }
        });

        btnDone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if(isSMSBanking)
                    smsDialog.show();
                else
                    CheckOutAction();
            }
        });


        btnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Fragment newFragment = null;
                newFragment = new FragmentInvUserList();
                Bundle args = new Bundle();
                args.putString("member_id", member_id);
                args.putString("member_code", member_code);
                args.putString("member_name", member_name);
                args.putString("session_id_param", "null");

                args.putString("comm_id", comm_id);
                args.putString("comm_name", comm_name);
                args.putString("comm_code", comm_code);
                args.putString("buss_scheme_code", buss_scheme_code);

                newFragment.setArguments(args);
                switchFragment(newFragment);
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Fragment newFragment = null;
                newFragment = new FragmentMemberInvUser();
                Bundle args = new Bundle();
                args.putString("comm_id", comm_id);
                args.putString("comm_name", comm_name);
                args.putString("comm_code", comm_code);
                args.putString("buss_scheme_code", buss_scheme_code);

                newFragment.setArguments(args);
                switchFragment(newFragment);
            }
        });

        return view;
    }

    private void CheckOutAction(){
        if(payment_type_code.equalsIgnoreCase(AppParams.ESPAY)) {
            if (total_pay != null && !total_pay.equals("") && !total_pay.equals("null") && Double.parseDouble(total_pay) > 0 &&
                    !payment_type_code.equals("")) {

                PaymentTypeBean paymentSelected = (PaymentTypeBean) cbo_payment_type.getSelectedItem();
                payment_type_code = String.valueOf(paymentSelected.payment_type);
                payment_type_name = String.valueOf(paymentSelected.payment_name);

                String param_sales_id = AppHelper.getUserId(activity);

                Fragment newFragment = null;
                newFragment = new FragmentPreviewTransaksi();
                Bundle args = new Bundle();
                args.putString("login_type", AppParams.MEMBER);
                args.putString("comm_id", comm_id);
                args.putString("bank_code", payment_bankcode);
                args.putString("product_code", payment_productcode);
                args.putString("ccy_id", ccy_id);
                args.putString("session_id", session_id_param);
                args.putString("product_code", payment_productcode);
                args.putString("product_h2h", payment_product_h2h);
                args.putString("product_value", product_values);

                args.putString("member_id", member_id);
                args.putString("member_code", member_code);
                args.putString("member_name", member_name);
                args.putString("session_id_param", session_id_var);

                args.putString("comm_name", comm_name);
                args.putString("comm_code", comm_code);
                args.putString("buss_scheme_code", buss_scheme_code);
                args.putBoolean("is_smsbanking",isSMSBanking);

                newFragment.setArguments(args);
                switchFragment(newFragment);

            } else if (payment_type_code.equals("")) {
                Toast.makeText(getActivity(), "Checkout tidak dapat dilakukan bila belum ada item yang diisi!", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getActivity(), "Checkout tidak dapat dilakukan bila belum ada item yang diisi!", Toast.LENGTH_SHORT).show();
            }
        } else if (payment_type_code.equals("")) {
            Toast.makeText(getActivity(), "Checkout tidak dapat dilakukan bila belum ada item yang diisi!", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getActivity(), "Checkout tidak dapat dilakukan bila belum ada item yang diisi!", Toast.LENGTH_SHORT).show();
        }
    }

    public void parseJSONData(){
        clearData();
        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
        client.setTimeout(AplConstants.HTTP_LONG_TIMEOUT);
        RequestParams params = new RequestParams();

        params.put("member_id", member_id);
        params.put("session_id", session_id_param);

        Log.d("params", params.toString());
        Log.wtf("yang dihit",AplConstants.InvUserListMobileAPI);
        client.post(AplConstants.InvUserListMobileAPI, params, new AsyncHttpResponseHandler() {
            public void onSuccess(String content) {
                Log.d("result:", content);
                try {
                    // parse json data and store into arraylist variables
                    JSONObject json = new JSONObject(content);
                    String error_code         = json.getString("error_code");
                    String error_message      = json.getString("error_message");

                    if (error_code.equals(AppParams.SUCCESS_CODE)) {

                        String bankEspay = json.getString(AppParams.BANK_ESPAY);

                        SecurePreferences.Editor mEditor = sp.edit();
                        mEditor.putString(AppParams.BANK_ESPAY, bankEspay);
                        mEditor.apply();

                        String paymentType = json.getString(AppParams.PAYMENT_TYPE);
                        if(!paymentType.equals("")) {
                            JSONArray arrPaymentType = new JSONArray(paymentType);
                            PaymentTypeList.add(new PaymentTypeBean("0", getResources().getString(R.string.pilih_tipe_pembayaran)));
//                            for (int i = 0; i < arrPaymentType.length(); i++) {
//                                if(arrPaymentType.getJSONObject(i).getString(AppParams.PAYMENT_CODE).equalsIgnoreCase(AppParams.ESPAY))
//                                    PaymentTypeList.add(new PaymentTypeBean(arrPaymentType.getJSONObject(i).getString(AppParams.PAYMENT_CODE), arrPaymentType.getJSONObject(i).getString(AppParams.PAYMENT_NAME)));
//                            }
                            PaymentTypeList.add(new PaymentTypeBean("ESPAY","Espay"));

                        }
                        else {
                            PaymentTypeList.add(new PaymentTypeBean("0", getResources().getString(R.string.tidak_ada_data)));
                        }
                        PaymentTypeAdapter paymentTypeAdapter = new PaymentTypeAdapter(activity, android.R.layout.simple_spinner_item, PaymentTypeList){
                            @Override
                            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                                View v = null;

                                // If this is the initial dummy entry, make it hidden
                                if (position == 0) {
                                    TextView tv = new TextView(getContext());
                                    tv.setHeight(0);
                                    tv.setVisibility(View.GONE);
                                    v = tv;
                                }
                                else {
                                    // Pass convertView as null to prevent reuse of special case views
                                    v = super.getDropDownView(position, null, parent);
                                }

                                // Hide scroll bar because it appears sometimes unnecessarily, this does not prevent scrolling
                                parent.setVerticalScrollBarEnabled(false);
                                return v;
                            }
                        };

                        cbo_payment_type.setAdapter(paymentTypeAdapter);
                        cbo_payment_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                if(position == 0) {

                                }
                                else {
                                    if(alertShow) return;;
                                    String type = null;

                                    type = PaymentTypeList.get(position).getPayment_type();

                                    if (PaymentTypeList.get(position).getPayment_type().equalsIgnoreCase(payment_type_code)) {
                                        alertShow = true;
                                        FragmentManager fragmentManager = getFragmentManager();
                                        dialog = new DialogFragmentDGI();
                                        Bundle args = new Bundle();
                                        args.putString("payment_type", type);
                                        args.putString("payment_bank_code", payment_bankcode);
                                        args.putString("payment_product_code", payment_productcode);
                                        dialog.setArguments(args);
                                        dialog.setCancelable(false);
                                        dialog.setTargetFragment(FragmentInvUserList.this, 100);
                                        dialog.show(fragmentManager, "dialogDGI");
                                    }
                                    else {
                                        alertShow = true;
                                        FragmentManager fragmentManager = getFragmentManager();
                                        dialog = new DialogFragmentDGI();
                                        Bundle args = new Bundle();
                                        args.putString("payment_type", type);
                                        args.putString("payment_bank_code", "0");
                                        args.putString("payment_product_code", "0");
                                        dialog.setArguments(args);
                                        dialog.setCancelable(false);
                                        dialog.setTargetFragment(FragmentInvUserList.this, 100);
                                        dialog.show(fragmentManager, "dialogDGI");
                                    }

                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

                        JSONArray data = json.getJSONArray("invoice_data"); // this is the "items: [ ] part

                        partial_payment = json.getString("partial_payment");
                        total_pay       = json.getString("total_pay");
                        session_id_var  = json.getString("session_id_var");


                        String total_payment = (total_pay != null && !total_pay.equals("") && !total_pay.equals("null") && Double.parseDouble(total_pay) > 0 )  ? FormatCurrency.getRupiahFormat(total_pay) : "";
                        lbl_total_pay_amount.setText(total_payment);

                        for (int i = 0; i < data.length(); i++) {
                            JSONObject object = data.getJSONObject(i);
                            doc_no.add(object.getString("doc_no"));
                            doc_id.add(object.getString("doc_id"));
                            amount.add(object.getString("amount"));
                            hold_amount.add(object.getString("hold_amount"));
                            remain_amount.add(object.getString("remain_amount"));
                            ccy.add(object.getString("ccy"));
                            doc_desc.add(object.getString("doc_desc"));
                            input_amount.add(object.getString("input_amount"));
                            session_id.add(object.getString("session_id"));
                        }

                        // when finish parsing, hide progressbar
                        prgLoading.setVisibility(View.GONE);

                        // if data available show data on list
                        // otherwise, show alert text
                        if(doc_no.size() > 0){
                            listMenu.setVisibility(View.VISIBLE);
                            listMenu.setAdapter(invoicePaymentAdapter);
                            lbl_header.setVisibility(View.VISIBLE);
                            tabel_footer.setVisibility(View.VISIBLE);
                        }else{
                            txtAlert.setVisibility(View.VISIBLE);
                        }

                    }else{
                        prgLoading.setVisibility(View.GONE);
                        txtAlert.setVisibility(View.VISIBLE);
                        txtAlert.setText(error_message);
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();

                }
            };

            public void onFailure(Throwable error, String content) {
                Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
            }

            ;
        });
    }

    // clear arraylist variables before used
    void clearData(){
        doc_no.clear();
        doc_id.clear();
        amount.clear();
        remain_amount.clear();
        ccy.clear();
        doc_desc.clear();
        input_amount.clear();
        session_id.clear();
    }

    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
        }

    };

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
    }

    @Override
    public void onFinishDialog() {
        PaymentTypeBean paymentSelected  = (PaymentTypeBean) cbo_payment_type.getSelectedItem();
        payment_type_code   = String.valueOf(paymentSelected.payment_type);
        payment_type_name   = String.valueOf(paymentSelected.payment_name);
        alertShow = false;
    }

    @Override
    public void onFinishEditDialog(String inputText) {

    }

    @Override
    public void onFinishSpinnerBankDialog(String bankCode, String bankName) {
        payment_bankcode = bankCode;
        payment_bankname = bankName;
        alertShow = false;
    }

    @Override
    public void onFinishSpinnerProductDialog(String productCode, String productName, String product_h2h) {
        payment_productcode = productCode;
        payment_productname = productName;
        payment_product_h2h = product_h2h;
        alertShow = false;

        if (payment_product_h2h.equalsIgnoreCase("Y")) {
            if (productCode.equalsIgnoreCase("MANDIRISMS"))
            {
                initializeSMSBanking();
            }
        }
        else
            isSMSBanking = false;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(smSclass != null && isSMSBanking)
            getActivity().registerReceiver(simStateReceiver,simStateIntentFilter);

    }

    @Override
    public void onPause() {
        super.onPause();
        if(smSclass != null && isSMSBanking)
            getActivity().unregisterReceiver(simStateReceiver);
    }

    @Override
    public void onDismiss(boolean dismiss) {
        if(dismiss == true) {
            cbo_payment_type.setSelection(0);
            payment_bankcode = "";
            payment_bankname = "";
            alertShow = false;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if(isSMSBanking && smsDialog.isShowing()) {
            smsDialog.dismiss();
            smsDialog.reset();
        }
    }

    private void initializeSmsClass(){
        if(smSclass == null)
            smSclass = new SMSclass(getActivity());

        smSclass.isSimExists(new SMSclass.SMS_SIM_STATE() {
            @Override
            public void sim_state(Boolean isExist, String msg) {
                if(!isExist){
                    Toast.makeText(getActivity(),msg,Toast.LENGTH_SHORT).show();
//                    TopUpActivity.this.finish();
                    clearPaymentData();
                }
            }
        });

        if(smsDialog == null) {
            smsDialog = new SMSDialog(getActivity(), comm_id, new SMSDialog.DialogButtonListener() {
                @Override
                public void onClickOkButton(View v, boolean isLongClick) {
                    if(reqPermissionClass == null) {
                        reqPermissionClass = new ReqPermissionClass(getActivity());
                        reqPermissionClass.setTargetFragment(FragmentInvUserList.this);
                    }if (reqPermissionClass.checkPermission(Manifest.permission.SEND_SMS,ReqPermissionClass.PERMISSIONS_SEND_SMS)) {
                        smsDialog.sentSms(comm_id);
                    }
                }

                @Override
                public void onClickCancelButton(View v, boolean isLongClick) {
                }

                @Override
                public void onSuccess(int user_is_new) {

                }

                @Override
                public void onSuccess(String product_value) {
                    product_values = product_value;
                    CheckOutAction();
                }
            });
        }

        try{
            getActivity().unregisterReceiver(simStateReceiver);
        }
        catch (Exception e){}
        getActivity().registerReceiver(simStateReceiver,simStateIntentFilter);
    }

    private void initializeSMSBanking(){
        isSMSBanking = true;
        if(reqPermissionClass == null)
            reqPermissionClass = new ReqPermissionClass(getActivity());
        reqPermissionClass.setTargetFragment(this);
        if(reqPermissionClass.checkPermission(Manifest.permission.READ_PHONE_STATE,ReqPermissionClass.PERMISSIONS_REQ_READPHONESTATE)){
            initializeSmsClass();
        }
    }

    private void clearPaymentData(){
        payment_type_code = "";
        dialog.spinnerProduct.setSelection(0);
        cbo_payment_type.setSelection(0);
    }

    public BroadcastReceiver simStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equalsIgnoreCase("android.intent.action.SIM_STATE_CHANGED")) {
                if(intent.getStringExtra("ss").equalsIgnoreCase("ABSENT")){
                    if(smsDialog != null) {
                        if (smsDialog.isShowing()) {
                            smsDialog.dismiss();
                            smsDialog.reset();
                        }
                    }
                }

            }
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults){
        Log.d("permission", "request code "+requestCode);

        if (reqPermissionClass.checkOnPermissionResult(requestCode, grantResults, ReqPermissionClass.PERMISSIONS_SEND_SMS)) {
            smsDialog.sentSms(comm_id);
        }else {
            if(requestCode == ReqPermissionClass.PERMISSIONS_SEND_SMS) {
                Toast.makeText(getActivity(), getString(R.string.cancel_permission_read_contacts), Toast.LENGTH_SHORT).show();
                if (smsDialog != null) {
                    smsDialog.dismiss();
                    smsDialog.reset();
                }
            }
        }
        if (reqPermissionClass.checkOnPermissionResult(requestCode, grantResults, ReqPermissionClass.PERMISSIONS_REQ_READPHONESTATE)){
            Log.d("permission", "given read phone state permission");
            initializeSmsClass();
        }else{
            if(requestCode == ReqPermissionClass.PERMISSIONS_REQ_READPHONESTATE) {
                clearPaymentData();
            }
        }
    }
}
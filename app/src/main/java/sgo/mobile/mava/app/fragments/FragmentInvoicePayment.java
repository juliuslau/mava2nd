package sgo.mobile.mava.app.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.mava.AppHelper;
import sgo.mobile.mava.R;
import sgo.mobile.mava.app.activities.MainActivity;
import sgo.mobile.mava.app.ui.dialog.DefinedDialog;
import sgo.mobile.mava.conf.AplConstants;
import sgo.mobile.mava.conf.AppParams;
import sgo.mobile.mava.frameworks.math.FormatCurrency;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpClient;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpResponseHandler;
import sgo.mobile.mava.frameworks.net.loopj.android.http.MySSLSocketFactory;
import sgo.mobile.mava.frameworks.net.loopj.android.http.RequestParams;

public class FragmentInvoicePayment extends Fragment {
    private ProgressDialog pDialog;
    FragmentManager fm;

    public String member_id, member_code, member_name;
    public String doc_no, doc_id, amount, remain_amount, input_amount, ccy, partial, doc_desc, session_id;
    public String comm_id, comm_name, comm_code, sales_alias,buss_scheme_code;
    public String bank_code, bank_name, ccy_id, product_code, seller_fee, buyer_fee, tx_fee, member_phone, benef_acct_no, benef_acct_name, charges_acct_no, fee_acct_no;

    EditText inpAmount;
    Button btnDone;
    Button btnCancel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_invpayment, container, false);

        inpAmount             = (EditText) view.findViewById(R.id.inpAmount);
        btnDone               = (Button) view.findViewById(R.id.btnDone);
        btnCancel             = (Button) view.findViewById(R.id.btnCancel);

        final Bundle bundle   =  this.getArguments();
        doc_no                =  bundle.getString("doc_no");
        doc_id                =  bundle.getString("doc_id");
        amount                =  FormatCurrency.getRupiahFormat(bundle.getString("amount"));
        remain_amount         =  bundle.getString("remain_amount");
        remain_amount         =  FormatCurrency.getRupiahFormat(remain_amount);
        ccy                   =  bundle.getString("ccy");

        String str_partial    = bundle.getString("partial_payment");
        if(str_partial.equalsIgnoreCase("Y")) {
            partial = "Ya";
        }else if(str_partial.equalsIgnoreCase("N")){
            partial = "Tidak";
        }else{
            partial = "Bisa Lebih";
        }

        comm_id               = bundle.getString("comm_id");
        comm_name             = bundle.getString("comm_name");
        comm_code             = bundle.getString("comm_code");
        sales_alias           = bundle.getString("sales_alias");
        buss_scheme_code      = bundle.getString("buss_scheme_code");

        doc_desc              =  bundle.getString("doc_desc");
        if (doc_desc != null && !doc_desc.equals("") && !doc_desc.equals("null")){
            doc_desc              =  bundle.getString("doc_desc");
        }else{
            doc_desc              =  "";
        }

        member_id             =  bundle.getString("member_id");
        member_code           =  bundle.getString("member_code");
        member_name           =  bundle.getString("member_name");
        session_id            =  bundle.getString("session_id");

        bank_code              = bundle.getString("bank_code");
        bank_name              = bundle.getString("bank_name");
        product_code           = bundle.getString("product_code");
        ccy_id                 = bundle.getString("ccy_id");
        seller_fee             = bundle.getString("seller_fee");
        buyer_fee              = bundle.getString("buyer_fee");
        tx_fee                 = bundle.getString("tx_fee");
        member_phone           = bundle.getString("member_phone");
        benef_acct_no          = bundle.getString("benef_acct_no");
        benef_acct_name        = bundle.getString("benef_acct_name");
        charges_acct_no        = bundle.getString("charges_acct_no");
        fee_acct_no            = bundle.getString("fee_acct_no");

        TextView lbl_doc_no = (TextView) view.findViewById(R.id.lbl_doc_no);
        lbl_doc_no.setText(doc_no);

        TextView lbl_doc_desc = (TextView) view.findViewById(R.id.lbl_doc_desc);
        lbl_doc_desc.setText(doc_desc);

        TextView lbl_amount = (TextView) view.findViewById(R.id.lbl_amount);
        lbl_amount.setText(amount);

        TextView lbl_remain_amount = (TextView) view.findViewById(R.id.lbl_remain_amount);
        lbl_remain_amount.setText(remain_amount);

        TextView lbl_partial = (TextView) view.findViewById(R.id.lbl_partial);
        lbl_partial.setText(partial);

        TextView lbl_fee = (TextView) view.findViewById(R.id.lbl_fee);
        lbl_fee.setText(FormatCurrency.getRupiahFormat(buyer_fee));

        if(partial.equalsIgnoreCase("Ya") || partial.equalsIgnoreCase("Bisa Lebih")){
            showInvoiceAmount(view);
        }else{
            hideInvoiceAmount(view);
        }

        TextView lbl_bank = (TextView) view.findViewById(R.id.lbl_bank);
        lbl_bank.setText(bank_name);

        TextView lbl_bank_product = (TextView) view.findViewById(R.id.lbl_bank_product);
        lbl_bank_product.setText(product_code);

        btnDone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                input_amount = inpAmount.getText().toString();
                if(partial.equalsIgnoreCase("Ya") || partial.equalsIgnoreCase("Bisa Lebih")){
                    input_amount = inpAmount.getText().toString();
                }else{
                    input_amount = bundle.getString("remain_amount");
                }
                if(input_amount.equalsIgnoreCase(""))
                {
                    Toast.makeText(getActivity(), R.string.form_alert, Toast.LENGTH_SHORT).show();
                }else{

                    String param_sales_id = AppHelper.getUserId(getActivity());
                    pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, "Validasi jumlah pembayaran...");
                    AsyncHttpClient client = new AsyncHttpClient();
                    client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
                    client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
                    RequestParams params   = new RequestParams();

                    params.put("member_id", member_id);
                    params.put("sales_id", param_sales_id);
                    params.put("ccy_id", ccy);
                    params.put("bank_code", bank_code);
                    params.put("product_code", product_code);
                    params.put("doc_id", doc_id);
                    params.put("amount", input_amount);

                    Log.d("params", params.toString());
                    client.post(AplConstants.InvValidateMobilAPI, params, new AsyncHttpResponseHandler() {
                        public void onSuccess(String content) {
                            Log.d("result:", content);
                            try {
                                JSONObject object         = new JSONObject(content);

                                String error_code         = object.getString("error_code");
                                String error_msg          = object.getString("error_message");

                                if (pDialog != null) {
                                    pDialog.dismiss();
                                }

                                if (error_code.equals(AppParams.SUCCESS_CODE)) {

                                    JSONArray dataInv    = object.getJSONArray("invoice_data");
                                    JSONObject objectInv = dataInv.getJSONObject(0);
                                    String docStatus  = objectInv.getString("doc_status");
                                    String docMessage = objectInv.getString("doc_remark");

                                    if(docStatus.equalsIgnoreCase("S")){

                                        pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, "Menyimpan Pembayaran...");
                                        AsyncHttpClient client = new AsyncHttpClient();
                                        client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
                                        client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
                                        RequestParams params   = new RequestParams();

                                        params.put("doc_id", doc_id);
                                        params.put("doc_no", doc_no);
                                        params.put("amount", input_amount);
                                        params.put("fee_amount", buyer_fee);
                                        params.put("session_id", session_id);
                                        Log.d("params", params.toString());
                                        client.post(AplConstants.InvSavePayMobileAPI, params, new AsyncHttpResponseHandler() {
                                            public void onSuccess(String content) {
                                                Log.d("result:", content);
                                                try {
                                                    JSONObject object         = new JSONObject(content);

                                                    String error_code         = object.getString("error_code");
                                                    String error_msg          = object.getString("error_message");

                                                    if (pDialog != null) {
                                                        pDialog.dismiss();
                                                    }

                                                    hideKeyboard();

                                                    Fragment newFragment = null;
                                                    newFragment = new FragmentInvoiceListPayment();
                                                    Bundle args = new Bundle();
                                                    args.putString("member_id", member_id);
                                                    args.putString("member_code", member_code);
                                                    args.putString("member_name", member_name);
                                                    args.putString("session_id_param", session_id);

                                                    args.putString("comm_id", comm_id);
                                                    args.putString("comm_name", comm_name);
                                                    args.putString("comm_code", comm_code);
                                                    args.putString("sales_alias", sales_alias);
                                                    args.putString("buss_scheme_code", buss_scheme_code);

                                                    newFragment.setArguments(args);
                                                    switchFragment(newFragment);


                                                } catch (JSONException e) {
                                                    // TODO Auto-generated catch block
                                                    e.printStackTrace();
                                                }
                                            };

                                            public void onFailure(Throwable error, String content) {
                                                if (pDialog != null) {
                                                    pDialog.dismiss();
                                                }
                                                Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                                            }
                                        });

                                    }else{
                                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                        alert.setTitle("Invoice Payment");
                                        alert.setMessage("Invoice Payment : " + docMessage);
                                        alert.setPositiveButton("OK", null);
                                        alert.show();
                                    }


                                } else {
                                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                    alert.setTitle("Invoice Payment");
                                    alert.setMessage("Invoice Payment : " + error_msg);
                                    alert.setPositiveButton("OK", null);
                                    alert.show();
                                }

                            } catch (JSONException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        };
                        public void onFailure(Throwable error, String content) {
                            if (pDialog != null) {
                                pDialog.dismiss();
                            }
                            Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                hideKeyboard();
                Fragment newFragment = null;
                newFragment = new FragmentInvoiceListPayment();
                Bundle args = new Bundle();
                args.putString("member_id", member_id);
                args.putString("member_code", member_code);
                args.putString("member_name", member_name);
                args.putString("session_id_param", session_id);

                args.putString("comm_id", comm_id);
                args.putString("comm_name", comm_name);
                args.putString("comm_code", comm_code);
                args.putString("sales_alias", sales_alias);
                args.putString("buss_scheme_code", buss_scheme_code);

                newFragment.setArguments(args);
                switchFragment(newFragment);
            }
        });

        return view;
    }

    public void showInvoiceAmount(View target){
        int TableId = R.id.tableInvoiceAmount;
        TableRow TableVisibility = (TableRow) target.findViewById(TableId);
        TableVisibility.setVisibility(View.VISIBLE);

        int lineDividerId = R.id.line_divider_amount;
        View lineDivider = (View) target.findViewById(lineDividerId);
        lineDivider.setVisibility(View.VISIBLE);
    }

    public void hideInvoiceAmount(View target){
        int TableId = R.id.tableInvoiceAmount;
        TableRow TableVisibility = (TableRow) target.findViewById(TableId);
        TableVisibility.setVisibility(View.GONE);

        int lineDividerId = R.id.line_divider_amount;
        View lineDivider = (View) target.findViewById(lineDividerId);
        lineDivider.setVisibility(View.GONE);
    }

    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
        }

    };

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
    }

    public void hideKeyboard(){
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

}
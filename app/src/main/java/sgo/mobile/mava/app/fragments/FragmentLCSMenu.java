package sgo.mobile.mava.app.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import sgo.mobile.mava.conf.AplConstants;
import sgo.mobile.mava.frameworks.net.loopj.android.http.MySSLSocketFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.mava.AppHelper;
import sgo.mobile.mava.R;
import sgo.mobile.mava.app.activities.MainActivity;
import sgo.mobile.mava.app.adapter.MenuLCSAdapter;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpClient;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpResponseHandler;
import sgo.mobile.mava.frameworks.net.loopj.android.http.RequestParams;

import java.util.ArrayList;

public class FragmentLCSMenu extends Fragment {
    private ProgressDialog pDialog;
    FragmentManager fm;

    // declare view objects
    private ListView listMenu;
    private ProgressBar prgLoading;
    private TextView txtAlert;
    private TextView lbl_header;

    MenuLCSAdapter menuLCSAdapter;

    // create arraylist variables to store data from server
    public static ArrayList<String> menu_setting_id          = new ArrayList<String>();
    public static ArrayList<String> menu_setting_code        = new ArrayList<String>();
    public static ArrayList<String> menu_setting_name        = new ArrayList<String>();
    public static ArrayList<String> menu_setting_description = new ArrayList<String>();

    public String user_type;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view  = inflater.inflate(R.layout.fragment_menu_setting_list, container, false);
        prgLoading = (ProgressBar) view.findViewById(R.id.prgLoading);
        listMenu   = (ListView) view.findViewById(R.id.listMenu);
        lbl_header = (TextView) view.findViewById(R.id.label_header);
        txtAlert = (TextView) view.findViewById(R.id.txtAlert);

        menuLCSAdapter = new MenuLCSAdapter(getActivity());
        parseJSONData();

        // event listener to handle list when clicked
        listMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                    long arg3) {
                if(menu_setting_code.get(position).equals("008"))
                {
                    Fragment newFragment = null;
                    newFragment = new FragmentCommunityInvPayment();
                    switchFragment(newFragment);
                }else if(menu_setting_code.get(position).equals("009")){
                    Fragment newFragment = null;
                    newFragment = new FragmentCommunityListDGI();
                    switchFragment(newFragment);
                }else if(menu_setting_code.get(position).equals("010")){
                    Fragment newFragment = null;
                    newFragment = new FragmentCommunityCCL();
                    switchFragment(newFragment);
                }else if(menu_setting_code.get(position).equals("013")){
                    Fragment newFragment = null;
                    newFragment = new FragmentCommunityCCLDI();
                    switchFragment(newFragment);
                }else if(menu_setting_code.get(position).equals("014")){
                    Fragment newFragment = null;
                    newFragment = new FragmentCommunityInvUser();
                    Bundle bundle = new Bundle();
                    bundle.putString("title", menu_setting_name.get(position));
                    newFragment.setArguments(bundle);
                    switchFragment(newFragment);
                }else{
                    Toast.makeText(getActivity(), "Mohon Maaf\nMenu '" + menu_setting_name.get(position) + "' untuk saat ini belum tersedia.", Toast.LENGTH_LONG).show();
                }
            }
        });


        return view;
    }

    public void parseJSONData(){
        clearData();
        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
        client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
        RequestParams params = new RequestParams();

        String loginType = AppHelper.getUserType(getActivity());
        if (loginType.equalsIgnoreCase("Member / Agent")) {
            user_type = "1";
        }else{
            user_type = "2";
        }
        params.put("user_type", user_type);
        Log.d("params", params.toString());
        client.get(AplConstants.ListMenuLCSMobileAPI, params, new AsyncHttpResponseHandler() {
        public void onSuccess(String content) {
                Log.d("result:", content);
                try {
                    // parse json data and store into arraylist variables
                    JSONObject json = new JSONObject(content);
                    JSONArray data = json.getJSONArray("menu_setting_data"); // this is the "items: [ ] part

                    for (int i = 0; i < data.length(); i++) {
                        JSONObject object = data.getJSONObject(i);
                        menu_setting_id.add(object.getString("menu_setting_id"));
                        menu_setting_code.add(object.getString("menu_setting_code"));
                        menu_setting_name.add(object.getString("menu_setting_name"));
                        menu_setting_description.add(object.getString("menu_setting_description"));
                    }

                    // when finish parsing, hide progressbar
                    prgLoading.setVisibility(8);

                    // if data available show data on list
                    // otherwise, show alert text
                    if (menu_setting_id.size() > 0) {
                        listMenu.setVisibility(0);
                        listMenu.setAdapter(menuLCSAdapter);
                        lbl_header.setVisibility(0);
                    } else {
                        txtAlert.setVisibility(0);
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();

                }
            }

            ;

            public void onFailure(Throwable error, String content) {
                Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
            }

            ;
        });
    }

    // clear arraylist variables before used
    void clearData(){
        menu_setting_id.clear();
        menu_setting_code.clear();
        menu_setting_name.clear();
        menu_setting_description.clear();
    }

    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
        }

    };

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
    }

}
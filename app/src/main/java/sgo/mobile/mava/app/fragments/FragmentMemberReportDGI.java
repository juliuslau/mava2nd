package sgo.mobile.mava.app.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;
import in.srain.cube.views.ptr.header.StoreHouseHeader;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.mava.AppHelper;
import sgo.mobile.mava.R;
import sgo.mobile.mava.app.activities.MainActivity;
import sgo.mobile.mava.app.adapter.MemberDGIReportAdapter;
import sgo.mobile.mava.conf.AplConstants;
import sgo.mobile.mava.conf.AppParams;
import sgo.mobile.mava.frameworks.math.FormatCurrency;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpClient;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpResponseHandler;
import sgo.mobile.mava.frameworks.net.loopj.android.http.MySSLSocketFactory;
import sgo.mobile.mava.frameworks.net.loopj.android.http.RequestParams;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class FragmentMemberReportDGI extends Fragment {
    FragmentManager fm;

    public int shop_amount;
    public String comm_id, comm_name, comm_code, sales_alias, comm_ccy, comm_amount, date_from, date_to, payment_type;

    // declare view objects
    ListView listMenu;
    ProgressBar prgLoading;
    TextView txtAlert;
    TextView lbl_header, lbl_sales_id, lbl_sales, lbl_jumlah;
    TableLayout tabel_header;
    TableLayout tabel_footer;


    MemberDGIReportAdapter memberDGIAdapter;

    // create arraylist variables to store data from server

    public static ArrayList<String> member_id = new ArrayList<String>();
    public static ArrayList<String> member_shop = new ArrayList<String>();
    public static ArrayList<String> ccy_id = new ArrayList<String>();
    public static ArrayList<String> amount = new ArrayList<String>();

    // create price format
    DecimalFormat formatData = new DecimalFormat("#.##");

    Button btnBack;

    int page = 0;
    private PtrFrameLayout mPtrFrame;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view  = inflater.inflate(R.layout.fragment_member_dgi_report, container, false);
        txtAlert = (TextView) view.findViewById(R.id.txtAlert);
        prgLoading = (ProgressBar) view.findViewById(R.id.prgLoading);
        mPtrFrame = (PtrFrameLayout) view.findViewById(R.id.report_ptr_frame);
        listMenu   = (ListView) view.findViewById(R.id.listMenu);
        lbl_header = (TextView) view.findViewById(R.id.label_header);
        tabel_header = (TableLayout) view.findViewById(R.id.tabel_header);
        tabel_footer = (TableLayout) view.findViewById(R.id.tabel_footer);
        lbl_sales_id = (TextView) view.findViewById(R.id.lbl_sales_id);
        lbl_sales = (TextView) view.findViewById(R.id.lbl_sales);
        lbl_jumlah = (TextView) view.findViewById(R.id.lbl_jumlah);

        Bundle bundle         = this.getArguments();
        comm_id               = bundle.getString(AppParams.COMM_ID);
        comm_name             = bundle.getString(AppParams.COMM_NAME);
        comm_code             = bundle.getString(AppParams.COMM_CODE);
        sales_alias           = bundle.getString(AppParams.SALES_ALIAS);
        comm_ccy              = bundle.getString(AppParams.CCY_ID);
        comm_amount           = bundle.getString(AppParams.AMOUNT);
        date_from             = bundle.getString(AppParams.DATE_FROM);
        date_to               = bundle.getString(AppParams.DATE_TO);
        payment_type          = bundle.getString(AppParams.PAYMENT_TYPE);
        memberDGIAdapter = new MemberDGIReportAdapter(getActivity());
        parseJSONData();

        lbl_header = (TextView) view.findViewById(R.id.label_header);
        lbl_header.setText(comm_name);

        StoreHouseHeader header = new StoreHouseHeader(getActivity().getApplicationContext());
        header.setPadding(0, 20, 0, 20);
        header.setTextColor(Color.BLACK);
        header.initWithString("Updating...");

        mPtrFrame.setDurationToCloseHeader(1500);
        mPtrFrame.setHeaderView(header);
        mPtrFrame.addPtrUIHandler(header);
        mPtrFrame.setPtrHandler(new PtrHandler() {
            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                frame.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (txtAlert.getVisibility() == View.VISIBLE) {

                        } else {
                            page++;
                            parseJSONData();
                        }
                        mPtrFrame.refreshComplete();
                    }
                }, 1800);
            }

            @Override
            public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
                return !canScrollUp(((ListView) content)); // or cast with ListView
            }

            public boolean canScrollUp(View view) {
                if (android.os.Build.VERSION.SDK_INT < 14) {
                    if (view instanceof AbsListView) {
                        final AbsListView absListView = (AbsListView) view;
                        return absListView.getChildCount() > 0
                                && (absListView.getFirstVisiblePosition() > 0 || absListView
                                .getChildAt(0).getTop() < absListView.getPaddingTop());
                    } else {
                        return view.getScrollY() > 0;
                    }
                } else {
                    return ViewCompat.canScrollVertically(view, -1);
                }
            }
        });

        // event listener to handle list when clicked
        listMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                    long arg3) {
                Fragment newFragment = new FragmentDetailReportDGI();
                Bundle args = new Bundle();
                args.putString(AppParams.COMM_ID, comm_id);
                args.putString(AppParams.COMM_NAME, comm_name);
                args.putString(AppParams.COMM_CODE, comm_code);
                args.putString(AppParams.SALES_ALIAS, sales_alias);
                args.putString(AppParams.COMM_CCY, comm_ccy);
                args.putString(AppParams.COMM_AMOUNT, comm_amount);

                args.putString(AppParams.MEMBER_ID, member_id.get(position));
                args.putString(AppParams.MEMBER_SHOP, member_shop.get(position));
                args.putString(AppParams.CCY_ID, ccy_id.get(position));
                args.putString(AppParams.AMOUNT, amount.get(position));

                args.putString(AppParams.DATE_FROM, date_from);
                args.putString(AppParams.DATE_TO, date_to);
                args.putString(AppParams.PAYMENT_TYPE, payment_type);

                clearData();
                newFragment.setArguments(args);
                getActivity().getSupportFragmentManager().popBackStack();
                switchFragment(newFragment);
            }
        });

        btnBack             = (Button) view.findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                clearData();
                Fragment newFragment = null;
                newFragment = new FragmentCommunityReportDGI();
                getActivity().getSupportFragmentManager().popBackStack();
                switchFragment(newFragment);
            }
        });

        return view;
    }

    public void parseJSONData(){
        try {
            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
            client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
            RequestParams params = new RequestParams();

            String param_sales_id = AppHelper.getUserId(getActivity());

            params.put(AppParams.COMM_ID, comm_id);
            params.put(AppParams.SALES_ID, param_sales_id);
            params.put(AppParams.CCY_ID, comm_ccy);
            params.put(AppParams.PAGE, Integer.toString(page));
            params.put(AppParams.DATE_FROM, date_from);
            params.put(AppParams.DATE_TO, date_to);
            params.put(AppParams.PAYMENT_TYPE, payment_type);

            Log.d("params", params.toString());
            client.post(AplConstants.DiReportMaster, params, new AsyncHttpResponseHandler() {
                public void onSuccess(String content) {
                    Log.d("result:", content);
                    try {
                        // parse json data and store into arraylist variables
                        JSONObject json = new JSONObject(content);
                        String error_code = json.getString("error_code");
                        String error_message = json.getString("error_message");

                        if (error_code.equals(AppParams.SUCCESS_CODE)) {
                            JSONArray data = json.getJSONArray("report_data"); // this is the "items: [ ] part
                            shop_amount = data.length();
                            for (int i = 0; i < shop_amount; i++) {
                                String id = data.getJSONObject(i).getString(AppParams.MEMBER_ID);
                                boolean flagSame = false;

                                // cek apakah ada member id yang sama.. kalau ada, tidak dimasukan ke array
                                if (member_id.size() > 0) {
                                    for (int index = 0; index < member_id.size(); index++) {
                                        if (member_id.get(index).equalsIgnoreCase(id)) {
                                            flagSame = true;
                                            break;
                                        } else {
                                            flagSame = false;
                                        }
                                    }
                                }

                                if (flagSame == false) {
                                    JSONObject object = data.getJSONObject(i);
                                    member_id.add(object.getString(AppParams.MEMBER_ID));
                                    member_shop.add(object.getString(AppParams.MEMBER_SHOP));
                                    ccy_id.add(object.getString(AppParams.CCY_ID));
                                    amount.add(object.getString(AppParams.AMOUNT));
                                }
                            }

                            // when finish parsing, hide progressbar
                            prgLoading.setVisibility(View.GONE);

                            // if data available show data on list
                            // otherwise, show alert text
                            if (member_id.size() > 0) {
                                listMenu.setVisibility(View.VISIBLE);
                                listMenu.setAdapter(memberDGIAdapter);
                                lbl_header.setVisibility(View.VISIBLE);
                                tabel_header.setVisibility(View.VISIBLE);
                                tabel_footer.setVisibility(View.VISIBLE);

                                lbl_sales_id.setText("Sales[" + sales_alias + "]");
                                lbl_sales.setText(Integer.toString(shop_amount) + " Toko");

                                Double dbl_amount = Double.parseDouble(comm_amount);
                                int int_amount = dbl_amount.intValue();
                                lbl_jumlah.setText(FormatCurrency.getRupiahFormat(Integer.toString(int_amount)));

                            } else {
                                txtAlert.setVisibility(View.VISIBLE);
                            }

                        } else if (member_id.size() == 0) {
                            prgLoading.setVisibility(View.GONE);
                            txtAlert.setVisibility(View.VISIBLE);
                            txtAlert.setText(error_message);
                        }

                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();

                    }
                }

                ;

                public void onFailure(Throwable error, String content) {
                    prgLoading.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                }

                ;
            });
        }
        catch (Exception e){
        }
    }

    // clear arraylist variables before used
    void clearData(){
        member_id.clear();
        member_shop.clear();
        ccy_id.clear();
        amount.clear();
    }

    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
        }

    };

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
    }

}
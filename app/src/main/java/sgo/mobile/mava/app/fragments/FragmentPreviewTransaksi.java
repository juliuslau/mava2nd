package sgo.mobile.mava.app.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.securepreferences.SecurePreferences;
import org.json.JSONException;
import org.json.JSONObject;

import sgo.mobile.mava.AppHelper;
import sgo.mobile.mava.R;
import sgo.mobile.mava.app.activities.MainActivity;
import sgo.mobile.mava.app.activities.SgoPlusWeb;
import sgo.mobile.mava.app.dialogs.DefinedDialog;
import sgo.mobile.mava.app.dialogs.DialogChoosePaymentKMK;
import sgo.mobile.mava.app.dialogs.ReportBillerDialog;
import sgo.mobile.mava.conf.AplConstants;
import sgo.mobile.mava.conf.AppParams;
import sgo.mobile.mava.frameworks.math.FormatCurrency;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpClient;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpResponseHandler;
import sgo.mobile.mava.frameworks.net.loopj.android.http.MySSLSocketFactory;
import sgo.mobile.mava.frameworks.net.loopj.android.http.RequestParams;

/**
 * Created by thinkpad on 8/7/2015.
 */
public class FragmentPreviewTransaksi extends Fragment implements ReportBillerDialog.OnDialogOkCallback, DialogChoosePaymentKMK.NoticeDialogListener {
    public static IntentFilter simStateIntentFilter = new IntentFilter("android.intent.action.SIM_STATE_CHANGED");

    View view;
    SecurePreferences sp;
    Activity activity;

    ProgressDialog out;

    LinearLayout layout_max_partial, layout_input_amount, layout_token;
    EditText et_input_amount, et_token;
    TextView tv_bank_name, tv_bank_product, tv_amount, tv_admin_fee, tv_total_amount, tv_max_partial;
    Button btnProcess, btnCancel;
    Boolean isSmsbanking = false;
    Boolean isMember = false;

    String member_id, member_code, member_name, session_id_param,
            comm_id, comm_name, comm_code, sales_alias,buss_scheme_code, api_key,
            sales_id, bank_code, product_code, product_value, ccy_id, session_id, product_h2h,
            tx_id, bank_name, product_name, tx_amount, tx_multiple, admin_fee, total_pay;
    String login_type, tx_kmk, flag_partial, rb_selected;
    Boolean isKmk = false;

    int start = 0;
    public static boolean changeFragment = false;
    AlertDialog alert = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String loginType = AppHelper.getUserType(getActivity());
        isMember = !loginType.equalsIgnoreCase("Member / Agent");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view  = inflater.inflate(R.layout.fragment_preview_transaksi, container, false);

        activity = getActivity();
        out = DefinedDialog.CreateProgressDialog(activity, null);

        sp = new SecurePreferences(activity);
        api_key = sp.getString(AppParams.API_KEY,"");

        layout_max_partial = (LinearLayout) view.findViewById(R.id.layout_max_partial);
        layout_input_amount = (LinearLayout) view.findViewById(R.id.layout_input_amount);
        layout_token = (LinearLayout) view.findViewById(R.id.layout_token);
        et_input_amount = (EditText) view.findViewById(R.id.input_amount_value);
        et_token = (EditText) view.findViewById(R.id.token_value);
        tv_bank_name = (TextView) view.findViewById(R.id.bank_name_value);
        tv_bank_product = (TextView) view.findViewById(R.id.bank_product_value);
        tv_amount = (TextView) view.findViewById(R.id.amount_value);
        tv_admin_fee = (TextView) view.findViewById(R.id.admin_fee_value);
        tv_total_amount = (TextView) view.findViewById(R.id.total_amount_value);
        tv_max_partial = (TextView) view.findViewById(R.id.max_partial_value);
        btnProcess = (Button) view.findViewById(R.id.preview_btn_process);
        btnCancel = (Button) view.findViewById(R.id.preview_btn_cancel);

        Bundle bundle = getArguments();
        if(bundle != null) {
            login_type = bundle.getString("login_type");
            comm_id = bundle.getString("comm_id");
            if(login_type.equalsIgnoreCase(AppParams.SALES)) sales_id = bundle.getString("sales_id");
            bank_code = bundle.getString("bank_code");
            product_code = bundle.getString("product_code");
            isSmsbanking = bundle.getBoolean("is_smsbanking");
            ccy_id = bundle.getString("ccy_id");
            session_id = bundle.getString("session_id");
            product_h2h = bundle.getString("product_h2h");
            if (isSmsbanking)
                product_value = bundle.getString("product_value");

            member_id             = bundle.getString("member_id");
            member_code           = bundle.getString("member_code");
            member_name           = bundle.getString("member_name");
            session_id_param      = bundle.getString("session_id_param");
            session_id_param      = (session_id_param.equalsIgnoreCase("null")) ? "" : session_id_param;

            comm_name             = bundle.getString("comm_name");
            comm_code             = bundle.getString("comm_code");
            sales_alias           = bundle.getString("sales_alias");
            buss_scheme_code      = bundle.getString("buss_scheme_code");

            getData();
        }

        btnProcess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnProcess.setEnabled(false);
                btnProcess.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        btnProcess.setEnabled(true);
                    }
                }, 3000);

                if(login_type.equalsIgnoreCase(AppParams.MEMBER)) {
                    if(isKmk) {
                        if (tx_kmk.equalsIgnoreCase("N")) {
                            setIfTxKmkN();
                        } else if (tx_kmk.equalsIgnoreCase("Y")) {
                            setIfTxKmkY();
                        } else if (tx_kmk.equalsIgnoreCase("B")) {
                            if (rb_selected.equalsIgnoreCase(getResources().getString(R.string.kmk))) {
                                setIfTxKmkY();
                            } else if (rb_selected.equalsIgnoreCase(getResources().getString(R.string.saving_giro))) {
                                setIfTxKmkN();
                            }
                        } else {
                            Toast.makeText(activity, getResources().getString(R.string.tidak_dapat_diproses), Toast.LENGTH_LONG).show();
                        }
                    }
                    else {
                        setIfTxKmkN();
                    }
                }
                else {
                    setIfTxKmkN();
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                backToInvoiceList();
            }
        });

        return view;
    }

    public void setIfTxKmkY() {
        if (product_h2h.equalsIgnoreCase("Y")) {
            if (layout_input_amount.getVisibility() == View.VISIBLE && inputAmountValidation() && layout_token.getVisibility() == View.GONE) {
                requestTokenKMK(et_input_amount.getText().toString());
            } else if (layout_token.getVisibility() == View.VISIBLE) {
                if (inputTokenValidation()) {
                    confirmTokenKMK(et_token.getText().toString());
                }
            }
        } else if (product_h2h.equalsIgnoreCase("N")) {

        } else {
            Toast.makeText(activity, getResources().getString(R.string.tidak_dapat_diproses), Toast.LENGTH_LONG).show();
        }
    }

    public void setIfTxKmkN() {
        if (product_h2h.equalsIgnoreCase("N")) {
            Intent i = new Intent(activity, SgoPlusWeb.class);
            i.putExtra(AppParams.REPAYMENT, "N");
            i.putExtra(AppParams.PRODUCT_CODE, product_code);
            i.putExtra(AppParams.PRODUCT_NAME, product_name);
            i.putExtra(AppParams.BANK_CODE, bank_code);
            i.putExtra(AppParams.BANK_NAME, bank_name);
            i.putExtra(AppParams.COMM_CODE, comm_code);
            i.putExtra(AppParams.TX_ID, tx_id);
            i.putExtra(AppParams.COMM_ID, comm_id);
            i.putExtra(AppParams.API_KEY, api_key);
            i.putExtra(AppParams.TX_AMOUNT, tx_amount);
            i.putExtra(AppParams.ADMIN_FEE, admin_fee);
            i.putExtra(AppParams.TOTAL_PAY, total_pay);
            i.putExtra(AppParams.TX_MULTIPLE, tx_multiple);

            switchActivity(i);
        } else if (product_h2h.equalsIgnoreCase("Y")) {
            if (inputTokenValidation()) {
                confirmToken(et_token.getText().toString());
            }
        } else {
            Toast.makeText(activity, getResources().getString(R.string.tidak_dapat_diproses), Toast.LENGTH_LONG).show();
        }
    }

    public void backToInvoiceList(){
        hideKeyboard();
        Fragment newFragment = null;
        if(login_type.equalsIgnoreCase(AppParams.SALES)) {
            newFragment = new FragmentInvoiceListDGI();
            Bundle args = new Bundle();
            args.putString("member_id", member_id);
            args.putString("member_code", member_code);
            args.putString("member_name", member_name);
            args.putString("session_id_param", session_id_param);

            args.putString("comm_id", comm_id);
            args.putString("comm_name", comm_name);
            args.putString("comm_code", comm_code);
            args.putString("sales_alias", sales_alias);
            args.putString("buss_scheme_code", buss_scheme_code);
            newFragment.setArguments(args);
            switchFragment(newFragment);
        }
        else if(login_type.equalsIgnoreCase(AppParams.MEMBER)){
            newFragment = new FragmentInvUserList();
            Bundle args = new Bundle();
            args.putString("member_id", member_id);
            args.putString("member_code", member_code);
            args.putString("member_name", member_name);
            args.putString("session_id_param", session_id_param);

            args.putString("comm_id", comm_id);
            args.putString("comm_name", comm_name);
            args.putString("comm_code", comm_code);
            args.putString("buss_scheme_code", buss_scheme_code);
            newFragment.setArguments(args);
            switchFragment(newFragment);
        }

    }

    @Override
    public void onStart() {
        super.onStart();

        if(start == 0) {
            start++;
            changeFragment = false;
        }
        if(changeFragment == false) {

        }
        else {
            if(alert != null){
                if (alert.isShowing()) {
                    alert.dismiss();
                }
            }
            if (login_type.equalsIgnoreCase(AppParams.SALES)) {
                Fragment newFragment = new FragmentMemberListDGI();
                Bundle args = new Bundle();
                args.putString("comm_id", comm_id);
                args.putString("comm_name", comm_name);
                args.putString("comm_code", comm_code);
                args.putString("sales_alias", sales_alias);
                args.putString("buss_scheme_code", buss_scheme_code);

                newFragment.setArguments(args);
                switchFragment(newFragment);
            } else if (login_type.equalsIgnoreCase(AppParams.MEMBER)) {
                Fragment newFragment = new FragmentMemberInvUser();
                Bundle args = new Bundle();
                args.putString("comm_id", comm_id);
                args.putString("comm_name", comm_name);
                args.putString("comm_code", comm_code);
                args.putString("buss_scheme_code", buss_scheme_code);

                newFragment.setArguments(args);
                switchFragment(newFragment);
            }
        }
    }

    public boolean inputAmountValidation(){
        if(et_input_amount.getText().toString().length()==0 || Long.parseLong(et_input_amount.getText().toString()) <= 0){
            et_input_amount.requestFocus();
            et_input_amount.setError(this.getString(R.string.validation_amount));
            return false;
        }
        return true;
    }

    public boolean inputTokenValidation(){
        if(et_token.getText().toString().length()==0){
            et_token.requestFocus();
            et_token.setError(this.getString(R.string.validation_otp));
            return false;
        }
        return true;
    }

    public void getData(){
        try {
            out.show();
            AsyncHttpClient client = new AsyncHttpClient();
            client.setTimeout(AplConstants.HTTP_LONG_TIMEOUT);
            client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());

            RequestParams params = new RequestParams();
            params.put(AppParams.COMM_ID, comm_id);
            if(login_type.equalsIgnoreCase(AppParams.SALES)) params.put(AppParams.SALES_ID, sales_id);
            if(login_type.equalsIgnoreCase(AppParams.MEMBER)) params.put(AppParams.MEMBER_ID, member_id);
            params.put(AppParams.BANK_CODE, bank_code);
            params.put(AppParams.PRODUCT_CODE, product_code);
            params.put(AppParams.CCY_ID, ccy_id);
            params.put(AppParams.SESSION_ID, session_id);

            Log.d("get data params", params.toString());

            String url = null;
            if(login_type.equalsIgnoreCase(AppParams.SALES)) url = AplConstants.DGIPaymentAPI;
            if(login_type.equalsIgnoreCase(AppParams.MEMBER)) url = AplConstants.PaymentMemberAPI;

            client.post(url, params, new AsyncHttpResponseHandler() {
                public void onSuccess(String content) {
                    Log.d("get data response:", content);
                    try {
                        out.dismiss();
                        JSONObject json = new JSONObject(content);

                        String error_code = json.getString("error_code");
                        String error_msg = json.getString("error_message");

                        if (error_code.equals(AppParams.SUCCESS_CODE)) {
                            tx_id = json.getString(AppParams.TX_ID);
                            bank_name = json.getString(AppParams.BANK_NAME);
                            product_name = json.getString(AppParams.PRODUCT_NAME);
                            tx_amount = json.getString(AppParams.TX_AMOUNT);
                            tx_multiple = json.getString(AppParams.TX_MULTIPLE);
                            admin_fee = json.getString(AppParams.ADMIN_FEE);

                            total_pay = Long.toString(Long.parseLong(tx_amount) + Long.parseLong(admin_fee));
                            tv_bank_name.setText(bank_name);
                            tv_bank_product.setText(product_name);
                            tv_amount.setText(FormatCurrency.getRupiahFormat(tx_amount));
                            tv_admin_fee.setText(FormatCurrency.getRupiahFormat(admin_fee));
                            tv_total_amount.setText(FormatCurrency.getRupiahFormat(total_pay));

                            if(login_type.equalsIgnoreCase(AppParams.MEMBER)) {
                                if(json.has(AppParams.TX_KMK)) {
                                    isKmk = true;
                                    tx_kmk = json.getString(AppParams.TX_KMK);

                                    if (tx_kmk.equalsIgnoreCase("N")) {
                                        if (product_h2h.equalsIgnoreCase("Y")) {
                                            layout_token.setVisibility(View.VISIBLE);
                                            getToken();
                                        }
                                    } else if (tx_kmk.equalsIgnoreCase("Y")) {
                                        if (product_h2h.equalsIgnoreCase("Y")) {
                                            inquiryTrxKMK();
                                        }
                                    } else if (tx_kmk.equalsIgnoreCase("B")) {
                                        FragmentManager fragmentManager = getFragmentManager();
                                        DialogChoosePaymentKMK dialog = new DialogChoosePaymentKMK();
                                        dialog.setCancelable(false);
                                        dialog.setTargetFragment(FragmentPreviewTransaksi.this, 100);
                                        dialog.show(fragmentManager, "dialogChoosePayment");
                                    } else {
                                        Toast.makeText(activity, getResources().getString(R.string.tx_kmk_tidak_sesuai), Toast.LENGTH_LONG).show();
                                    }
                                }
                                else {
                                    isKmk = false;
                                    if (product_h2h.equalsIgnoreCase("Y")) {
                                        layout_token.setVisibility(View.VISIBLE);
                                        getToken();
                                    }
                                }
                            }
                            else {
                                if (product_h2h.equalsIgnoreCase("Y")) {
                                    layout_token.setVisibility(View.VISIBLE);
                                    getToken();
                                }else
                                    layout_token.setVisibility(View.GONE);
                            }
                        }
                        else {
                            AlertDialog.Builder dialog = new AlertDialog.Builder(view.getContext());
                            dialog.setTitle("Alert");
                            dialog.setMessage(error_msg);
                            dialog.setCancelable(false);
                            dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    backToInvoiceList();
                                }
                            });
                            alert = dialog.create();
                            alert.show();
                        }
                    }
                    catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                };

                public void onFailure(Throwable error, String content) {
                    Toast.makeText(activity, "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                }
            });
        }catch (Exception e){
            Log.d("httpclient", e.getMessage());
        }
    }

    public void inquiryTrxKMK(){
        try {
            out.show();
            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
            client.setTimeout(AplConstants.HTTP_LONG_TIMEOUT);

            RequestParams params = new RequestParams();
            params.put("tx_id", tx_id);
            params.put("product_code", product_code);
            params.put("comm_code", comm_code);

            Log.d("params", params.toString());
            client.post(AplConstants.InquiryTrxKMKAPI, params, new AsyncHttpResponseHandler() {
                public void onSuccess(String content) {
                    Log.d("result:", content);
                    try {
                        // parse json data and store into arraylist variables
                        JSONObject json = new JSONObject(content);
                        String error_code = json.getString("error_code");
                        String error_message = json.getString("error_message");

                        out.dismiss();

                        if (error_code.equals(AppParams.SUCCESS_CODE)) {
                            String amount = json.getString(AppParams.AMOUNT);

//                            tv_tx_id.setText(json.getString(AppParams.TX_ID));
//                            tv_comm_name.setText(comm_name);
//                            tv_admin_fee.setText(json.getString(AppParams.ADMIN_FEE));
//                            tv_amount.setText(amount);
//                            tv_bank_name.setText(bank_name);
//                            tv_bank_product.setText(product_name);
//                            tv_ccy_id.setText(json.getString(AppParams.CCY_ID));
//                            tv_desc.setText(json.getString(AppParams.DESCRIPTION));

                            flag_partial = json.getString(AppParams.FLAG_PARTIAL);
                            String max_partial = json.getString(AppParams.MAX_PARTIAL);

                            if(flag_partial.equalsIgnoreCase("Y")) {
                                layout_max_partial.setVisibility(View.VISIBLE);
                                layout_input_amount.setVisibility(View.VISIBLE);
                                tv_max_partial.setText(max_partial);
                            }
                            else {
                                requestTokenKMK("0");
                            }

                        }
                        else if(error_code.equals("0003") || error_code.equals("0059") || error_code.equals("0031")){
                            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                            builder.setMessage(error_message)
                                    .setCancelable(false)
                                    .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            backToInvoiceList();
                                        }
                                    });
                            alert = builder.create();
                            alert.show();
                        }
                        else{
                            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                            builder.setMessage(error_message)
                                    .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            alert = builder.create();
                            alert.show();
                        }


                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();

                    }
                }

                ;

                public void onFailure(Throwable error, String content) {
                    Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                }

                ;
            });
        }catch (Exception e){
            Log.d("httpclient", e.getMessage());
        }
    }


    public void requestTokenKMK(String _amount){
        try {
            out.show();
            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
            client.setTimeout(AplConstants.HTTP_LONG_TIMEOUT);

            RequestParams params = new RequestParams();
            params.put("tx_id", tx_id);
            params.put("partial_amount", _amount);
            params.put("comm_code", comm_code);

            Log.d("params", params.toString());
            client.post(AplConstants.ReqTokenKMKAPI, params, new AsyncHttpResponseHandler() {
                public void onSuccess(String content) {
                    Log.d("result:", content);
                    try {
                        // parse json data and store into arraylist variables
                        JSONObject json = new JSONObject(content);
                        String error_code = json.getString("error_code");
                        String error_message = json.getString("error_message");

                        out.dismiss();

                        if (error_code.equals(AppParams.SUCCESS_CODE)) {
                            et_input_amount.setEnabled(false);
                            layout_token.setVisibility(View.VISIBLE);

                        }
                        else if (error_code.equals("0031")){
                            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                            builder.setMessage(error_message)
                                    .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            alert = builder.create();
                            alert.show();
                        }
                        else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                            builder.setMessage(error_message)
                                    .setCancelable(false)
                                    .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            backToInvoiceList();
                                        }
                                    });
                            alert = builder.create();
                            alert.show();
                        }


                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();

                    }
                }

                ;

                public void onFailure(Throwable error, String content) {
                    Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                }

                ;
            });
        }catch (Exception e){
            Log.d("httpclient", e.getMessage());
        }
    }

    public void confirmTokenKMK(String _token) {
        try {
            out.show();
            AsyncHttpClient client = new AsyncHttpClient();
            client.setTimeout(AplConstants.HTTP_LONG_TIMEOUT);
            client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());

            RequestParams params = new RequestParams();
            params.put(AppParams.TX_ID, tx_id);
            params.put(AppParams.COMM_CODE, comm_code);
            params.put(AppParams.TOKEN_ID, _token);

            Log.d("confirm token params", params.toString());
            client.post(AplConstants.ConfirmTokenKMKAPI, params, new AsyncHttpResponseHandler() {
                public void onSuccess(String content) {
                    Log.d("confirm token response:", content);
                    try {
                        out.dismiss();
                        JSONObject json = new JSONObject(content);

                        String error_code = json.getString("error_code");
                        String error_msg = json.getString("error_message");

                        if (error_code.equals(AppParams.SUCCESS_CODE)) {
                            onOkButton();
                        }
                        else if(error_code.equals("0031")) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                            builder.setTitle("Alert");
                            builder.setMessage(error_msg);
                            builder.setCancelable(false);
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    backToInvoiceList();
                                }
                            });
                            alert = builder.create();
                            alert.show();
                        }
                        else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                            builder.setTitle("Alert");
                            builder.setMessage(error_msg);
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            alert = builder.create();
                            alert.show();
                        }
                    }
                    catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                };

                public void onFailure(Throwable error, String content) {
                    Toast.makeText(activity, "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                }
            });
        }catch (Exception e){
            Log.d("httpclient", e.getMessage());
        }
    }


    public void getToken(){
        try {
            out.show();
            AsyncHttpClient client = new AsyncHttpClient();
            client.setTimeout(AplConstants.HTTP_LONG_TIMEOUT);
            client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());

            RequestParams params = new RequestParams();
//            params.put(AppParams.COMM_CODE, comm_code_sp);
            params.put(AppParams.COMM_CODE, comm_code);
            params.put(AppParams.TX_ID, tx_id);
            params.put(AppParams.PRODUCT_CODE, product_code);
            if (isSmsbanking)
                params.put(AppParams.PRODUCT_VALUE, product_value);

            Log.d("get token params", params.toString());
            client.post(AplConstants.InquiryTrxAPI, params, new AsyncHttpResponseHandler() {
                public void onSuccess(String content) {
                    Log.d("get token response:", content);
                    try {
                        out.dismiss();
                        JSONObject json = new JSONObject(content);

                        String error_code = json.getString("error_code");
                        String error_msg = json.getString("error_message");

                        if (error_code.equals(AppParams.SUCCESS_CODE)) {
                            //berhasil kirim token
                            layout_token.setVisibility(View.VISIBLE);
                        }
                        else {
                            AlertDialog.Builder dialog = new AlertDialog.Builder(view.getContext());
                            dialog.setTitle("Alert");
                            dialog.setMessage(error_msg);
                            dialog.setCancelable(false);
                            dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
//                                    dialog.dismiss();
                                    backToInvoiceList();
                                }
                            });
                            alert = dialog.create();
                            alert.show();
                        }
                    }
                    catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                };

                public void onFailure(Throwable error, String content) {
                    Toast.makeText(activity, "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                }
            });
        }catch (Exception e){
            Log.d("httpclient", e.getMessage());
        }
    }

    public void confirmToken(String token){
        try {
            out.show();
            AsyncHttpClient client = new AsyncHttpClient();
            client.setTimeout(AplConstants.HTTP_LONG_TIMEOUT);
            client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());

            RequestParams params = new RequestParams();
            params.put(AppParams.COMM_ID, comm_id);
            params.put(AppParams.TX_ID, tx_id);
//            params.put(AppParams.COMM_CODE, comm_code_sp);
            params.put(AppParams.COMM_CODE, comm_code);
            params.put(AppParams.PRODUCT_CODE, product_code);
            params.put(AppParams.PRODUCT_VALUE, token);
            params.put(AppParams.MEMBER_ID, member_id);

            Log.d("confirm token params", params.toString());
            client.post(AplConstants.InsertTrxAPI, params, new AsyncHttpResponseHandler() {
                public void onSuccess(String content) {
                    Log.d("confirm token response:", content);
                    try {
                        out.dismiss();
                        JSONObject json = new JSONObject(content);

                        String error_code = json.getString("error_code");
                        String error_msg = json.getString("error_message");

                        if (error_code.equals(AppParams.SUCCESS_CODE)) {
                            //berhasil confirm token
                            getTrxStatus();
                        }
                        else if(error_code.equals("0031")) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                            builder.setTitle("Alert");
                            builder.setMessage(error_msg);
                            builder.setCancelable(false);
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    backToInvoiceList();
                                }
                            });
                            alert = builder.create();
                            alert.show();
                        }
                        else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                            builder.setTitle("Alert");
                            builder.setMessage(error_msg);
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            alert = builder.create();
                            alert.show();
                        }
                    }
                    catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                };

                public void onFailure(Throwable error, String content) {
                    Toast.makeText(activity, "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                }
            });
        }catch (Exception e){
            Log.d("httpclient", e.getMessage());
        }
    }

    public void getTrxStatus(){
        try{
            out.show();
            AsyncHttpClient client = new AsyncHttpClient();
            client.setTimeout(AplConstants.HTTP_LONG_TIMEOUT);
            client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());

            RequestParams params = new RequestParams();
            params.put(AppParams.TX_ID, tx_id);
            params.put(AppParams.COMM_ID, comm_id);

            Log.d("params get Trx Status", params.toString());

            client.post(AplConstants.TrxStatusAPI, params, new AsyncHttpResponseHandler() {
                public void onSuccess(String content) {
                    try {
                        out.dismiss();
                        Log.d("response get Trx Status", content);

                        JSONObject response = new JSONObject(content);
                        String code = response.getString(AppParams.ERROR_CODE);
                        if (code.equals(AppParams.SUCCESS_CODE)) {
                            if(tx_multiple.equalsIgnoreCase("N")) {
                                String date = response.getString(AppParams.CREATED);
                                String tx_id = response.getString(AppParams.TX_ID);
                                String member_cust_id = response.getString(AppParams.MEMBER_CUST_ID);
                                String member_cust_name = response.getString(AppParams.MEMBER_CUST_NAME);
                                String bank_name = response.getString(AppParams.BANK_NAME);
                                String product_name = response.getString(AppParams.PRODUCT_NAME);
                                String tx_status = response.getString(AppParams.TX_STATUS);
                                String tx_remark = response.getString(AppParams.TX_REMARK);

                                showReportBillerDialog(date, tx_id, member_cust_id, member_cust_name, bank_name, product_name,
                                        tx_amount, admin_fee, total_pay, tx_status, tx_remark);
                            }
                            else if(tx_multiple.equalsIgnoreCase("Y")) {
                                String data_detail = response.getString(AppParams.DATA_DETAIL);
                                if(!data_detail.equals("")) {
                                    showReportActivity(data_detail, admin_fee);
                                }
                                else {
                                    String msg = "Detail Data is Empty";
                                    AlertDialog.Builder dialog = new AlertDialog.Builder(view.getContext());
                                    dialog.setTitle("Alert");
                                    dialog.setMessage(msg);
                                    dialog.setPositiveButton("OK", null);
                                    alert = dialog.create();
                                    alert.show();
                                }
                            }

                        } else {
                            String msg = response.getString(AppParams.ERROR_MESSAGE);
                            AlertDialog.Builder dialog = new AlertDialog.Builder(view.getContext());
                            dialog.setTitle("Alert");
                            dialog.setMessage(msg);
                            dialog.setPositiveButton("OK", null);
                            alert = dialog.create();
                            alert.show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                public void onFailure(Throwable error, String content) {
                    Toast.makeText(activity, "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                }
            });
        }catch (Exception e){
            Log.d("httpclient", e.getMessage());
        }
    }

    private void showReportBillerDialog(String date, String txId, String custID, String custName, String bankName,String productName,
                                        String amount, String fee, String total, String txStatus, String txRemark) {
        Bundle args = new Bundle();
        ReportBillerDialog dialog = new ReportBillerDialog();
        args.putString(AppParams.DATE_TIME,date);
        args.putString(AppParams.TX_ID, txId);
        args.putString(AppParams.MEMBER_CUST_ID, custID);
        args.putString(AppParams.MEMBER_CUST_NAME, custName);
        args.putString(AppParams.BANK_NAME, bankName);
        args.putString(AppParams.PRODUCT_NAME, productName);
        args.putString(AppParams.AMOUNT, amount);
        args.putString(AppParams.ADMIN_FEE, fee);
        args.putString(AppParams.TOTAL_PAY, total);
        args.putString(AppParams.TX_STATUS, txStatus);
        args.putString(AppParams.TX_REMARK, txRemark);
        dialog.setArguments(args);
        dialog.setTargetFragment(this, 0);
        dialog.show(getActivity().getSupportFragmentManager(), ReportBillerDialog.TAG);
    }

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
    }

    private void switchActivity(Intent mIntent){
        if (getActivity() == null)
            return;

        MainActivity fca = (MainActivity) getActivity();
        fca.switchActivity(mIntent, MainActivity.ACTIVITY_RESULT);
    }

    @Override
    public void onOkButton() {
        hideKeyboard();
        if(login_type.equalsIgnoreCase(AppParams.SALES)) {
            Fragment newFragment = new FragmentMemberListDGI();
            Bundle args = new Bundle();
            args.putString("comm_id", comm_id);
            args.putString("comm_name", comm_name);
            args.putString("comm_code", comm_code);
            args.putString("sales_alias", sales_alias);
            args.putString("buss_scheme_code", buss_scheme_code);

            newFragment.setArguments(args);
            switchFragment(newFragment);
        }
        else if(login_type.equalsIgnoreCase(AppParams.MEMBER)) {
            Fragment newFragment = new FragmentMemberInvUser();
            Bundle args = new Bundle();
            args.putString("comm_id", comm_id);
            args.putString("comm_name", comm_name);
            args.putString("comm_code", comm_code);
            args.putString("buss_scheme_code", buss_scheme_code);

            newFragment.setArguments(args);
            switchFragment(newFragment);
        }
    }

    private void showReportActivity(String data_detail, String fee){
        hideKeyboard();
        FragmentReportEspay fragment = new FragmentReportEspay();
        Bundle args = new Bundle();
        args.putString("data_detail", data_detail);
        args.putString("fee", fee);
        args.putString("type", "SMS");
        args.putString("login_type", login_type);
        args.putString("comm_id", comm_id);
        args.putString("comm_name", comm_name);
        args.putString("comm_code", comm_code);
        args.putString("sales_alias", sales_alias);
        args.putString("buss_scheme_code", buss_scheme_code);
        fragment.setArguments(args);
        switchFragment(fragment);
    }

    public void hideKeyboard(){
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onChoosePaymentKMK(String rbSelected) {
        rb_selected = rbSelected;
        if(rbSelected.equalsIgnoreCase(getResources().getString(R.string.kmk))) {
            if(product_h2h.equalsIgnoreCase("Y")) {
                inquiryTrxKMK();
            }
        }
        else if(rbSelected.equalsIgnoreCase(getResources().getString(R.string.saving_giro))) {
            if(product_h2h.equalsIgnoreCase("Y")) {
                layout_token.setVisibility(View.VISIBLE);
                getToken();
            }
        }
    }

    public BroadcastReceiver simStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equalsIgnoreCase("android.intent.action.SIM_STATE_CHANGED")) {
                if(intent.getStringExtra("ss").equalsIgnoreCase("ABSENT")){
                    if(getContext() instanceof Activity)
                        Toast.makeText(getActivity(), "Sim Card dicabut. Transaksi SMS Banking membutuhkan Sim Card.", Toast.LENGTH_LONG).show();
                        backToInvoiceList();
                }

            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        if (isMember && isSmsbanking)
            getActivity().registerReceiver(simStateReceiver,simStateIntentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (isMember && isSmsbanking)
            getActivity().unregisterReceiver(simStateReceiver);
    }
}

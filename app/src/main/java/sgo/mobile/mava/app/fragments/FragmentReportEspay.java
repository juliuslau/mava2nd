package sgo.mobile.mava.app.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import org.json.JSONArray;
import org.json.JSONException;
import sgo.mobile.mava.R;
import sgo.mobile.mava.app.activities.MainActivity;
import sgo.mobile.mava.app.adapter.ReportEspayAdapter;
import sgo.mobile.mava.app.beans.TrxStatusBean;
import sgo.mobile.mava.conf.AppParams;

import java.util.ArrayList;

/**
 * Created by thinkpad on 8/28/2015.
 */
public class FragmentReportEspay extends Fragment{

    Button btnOk;
    TextView tvCommName, tvMemberCode, tvBankName, tvProductName, tvFeeName;
    ListView lvTransaksi;
    ReportEspayAdapter reportEspayAdapter;
    ArrayList<TrxStatusBean> listTransaksi;
    //IB atau SMS
    String type;
    String login_type, comm_id, comm_name, comm_code, sales_alias, buss_scheme_code;

    public static FragmentReportEspay newInstance(String data_detail, String fee, String type) {
        FragmentReportEspay f = new FragmentReportEspay();

        Bundle args = new Bundle();
        args.putString("data_detail", data_detail);
        args.putString("fee", fee);
        args.putString("type",type);
        f.setArguments(args);

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_report_espay, container, false);

        listTransaksi = new ArrayList<TrxStatusBean>();

        tvCommName = (TextView) view.findViewById(R.id.comm_name_value);
        tvMemberCode = (TextView) view.findViewById(R.id.member_code_value);
        tvBankName = (TextView) view.findViewById(R.id.bank_name_value);
        tvProductName = (TextView) view.findViewById(R.id.product_name_value);
        tvFeeName = (TextView) view.findViewById(R.id.fee_value);
        lvTransaksi = (ListView) view.findViewById(R.id.lvTransaksi);
        btnOk = (Button) view.findViewById(R.id.report_btn_ok);

        Bundle bundle = getArguments();
        if(bundle != null) {
            String data_detail = bundle.getString("data_detail");
            String fee = bundle.getString("fee");
            type = bundle.getString("type");

            if(type.equalsIgnoreCase("SMS")) {
                login_type = bundle.getString("login_type");
                comm_id = bundle.getString("comm_id");
                comm_name = bundle.getString("comm_name");
                comm_code = bundle.getString("comm_code");
                sales_alias = bundle.getString("sales_alias");
                buss_scheme_code = bundle.getString("buss_scheme_code");
            }
            Log.d("detaill",data_detail);
            try {
                JSONArray arr_data_detail = new JSONArray(data_detail);
                for (int i = 0; i < arr_data_detail.length(); i++) {
                    TrxStatusBean trxStatusBean = new TrxStatusBean();
                    trxStatusBean.setTx_id(arr_data_detail.getJSONObject(i).getString(AppParams.TX_ID));
                    trxStatusBean.setTx_amount(arr_data_detail.getJSONObject(i).getString(AppParams.TX_AMOUNT));
                    trxStatusBean.setTx_status(arr_data_detail.getJSONObject(i).getString(AppParams.TX_STATUS));
                    trxStatusBean.setTx_date(arr_data_detail.getJSONObject(i).getString(AppParams.TX_DATE));
                    if(arr_data_detail.getJSONObject(i).getString(AppParams.TX_REMARK) == null || arr_data_detail.getJSONObject(i).getString(AppParams.TX_REMARK).equals("null"))
                        trxStatusBean.setTx_remark("");
                    else
                        trxStatusBean.setTx_remark(arr_data_detail.getJSONObject(i).getString(AppParams.TX_REMARK));
                    trxStatusBean.setBank_name(arr_data_detail.getJSONObject(i).getString(AppParams.BANK_NAME));
                    trxStatusBean.setComm_name(arr_data_detail.getJSONObject(i).getString(AppParams.COMM_NAME));
                    trxStatusBean.setMember_code(arr_data_detail.getJSONObject(i).getString(AppParams.MEMBER_CODE));
                    trxStatusBean.setProduct_name(arr_data_detail.getJSONObject(i).getString(AppParams.PRODUCT_NAME));
                    listTransaksi.add(trxStatusBean);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            tvCommName.setText(listTransaksi.get(0).getComm_name());
            tvMemberCode.setText(listTransaksi.get(0).getMember_code());
            tvBankName.setText(listTransaksi.get(0).getBank_name());
            tvProductName.setText(listTransaksi.get(0).getProduct_name());
            tvFeeName.setText(fee);
        }

        reportEspayAdapter = new ReportEspayAdapter(getActivity(), listTransaksi);
        lvTransaksi.setAdapter(reportEspayAdapter);
        lvTransaksi.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        setListViewHeightBasedOnChildren(lvTransaksi);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentPreviewTransaksi.changeFragment = true;
                if(type.equalsIgnoreCase("IB")) getActivity().finish();
                else if(type.equalsIgnoreCase("SMS")) {
                    if(login_type.equalsIgnoreCase(AppParams.SALES)) {
                        Fragment newFragment = new FragmentMemberListDGI();
                        Bundle args = new Bundle();
                        args.putString("comm_id", comm_id);
                        args.putString("comm_name", comm_name);
                        args.putString("comm_code", comm_code);
                        args.putString("sales_alias", sales_alias);
                        args.putString("buss_scheme_code", buss_scheme_code);

                        newFragment.setArguments(args);
                        switchFragment(newFragment);
                    }
                    else if(login_type.equalsIgnoreCase(AppParams.MEMBER)) {
                        Fragment newFragment = new FragmentMemberInvUser();
                        Bundle args = new Bundle();
                        args.putString("comm_id", comm_id);
                        args.putString("comm_name", comm_name);
                        args.putString("comm_code", comm_code);
                        args.putString("buss_scheme_code", buss_scheme_code);

                        newFragment.setArguments(args);
                        switchFragment(newFragment);
                    }
                }
            }
        });
        return view;
    }

    /**** Method for Setting the Height of the ListView dynamically.
     **** Hack to fix the issue of not showing all the items of the ListView
     **** when placed inside a ScrollView  ****/
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, AbsListView.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }


    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
    }
}

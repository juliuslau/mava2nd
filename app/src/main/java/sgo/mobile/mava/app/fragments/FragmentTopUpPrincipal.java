package sgo.mobile.mava.app.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import sgo.mobile.mava.conf.AplConstants;
import sgo.mobile.mava.conf.AppParams;
import sgo.mobile.mava.frameworks.net.loopj.android.http.MySSLSocketFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.mava.AppHelper;
import sgo.mobile.mava.R;
import sgo.mobile.mava.app.activities.MainActivity;

import sgo.mobile.mava.app.adapter.BankAdapter;
import sgo.mobile.mava.app.adapter.BankProductAdapter;
import sgo.mobile.mava.app.adapter.CommunityGWAdapter;
import sgo.mobile.mava.app.adapter.MemberGWAdapter;
import sgo.mobile.mava.app.beans.BankBean;
import sgo.mobile.mava.app.beans.BankProductBean;
import sgo.mobile.mava.app.beans.CommunityGWBean;
import sgo.mobile.mava.app.beans.MemberGWBean;

import sgo.mobile.mava.app.ui.dialog.DefinedDialog;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpClient;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpResponseHandler;
import sgo.mobile.mava.frameworks.net.loopj.android.http.RequestParams;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

public class FragmentTopUpPrincipal extends Fragment{
    private ProgressDialog pDialog;

    private String member_id, member_code, tx_id,tx_date, currency, amount, remark, bank_code, bank_name, product_code, product_name, comm_code, comm_name, benef_acct_no, benef_acct_name, tx_amount, buyer_fee, seller_fee, tx_fee, charges_acct_no, fee_acct_no;
    private String no_phone_source, data_inv, resend_token;

    private static Spinner cbo_bank      = null;
    private static Spinner cbo_bank_prod = null;
    private static Spinner cbo_community = null;
    private static Spinner cbo_member    = null;

    private EditText inpAmount;
    private EditText inpMemo;
    private Button btnLanjut;

    private String[] community_id_arr, community_name_arr, community_code_arr, buss_scheme_code_arr;
    private String[] member_id_arr, member_code_arr;
    private String[] bank_code_arr, bank_name_arr, ccy_id_arr, product_code_arr, seller_fee_arr, buyer_fee_arr, tx_fee_arr, member_phone_arr, benef_acct_no_arr, benef_acct_name_arr, charges_acct_no_arr, fee_acct_no_arr;
    private String[] bank_code_product_array, product_code_array, product_name_array;

    private ArrayList<CommunityGWBean> CommunityList = new ArrayList<CommunityGWBean>();
    private ArrayList<MemberGWBean> MemberList = new ArrayList<MemberGWBean>();
    private ArrayList<BankBean> BankList = new ArrayList<BankBean>();
    private ArrayList<BankProductBean> BankProductList = new ArrayList<BankProductBean>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_topup_principal, container, false);
        ImageButton btnAddSaldo         = (ImageButton) view.findViewById(R.id.btnAddSaldo);
        btnAddSaldo.setVisibility(View.GONE);

        cbo_community = (Spinner)view.findViewById(R.id.cbo_community);
        cbo_member    = (Spinner)view.findViewById(R.id.cbo_member);
        cbo_bank      = (Spinner)view.findViewById(R.id.cbo_bank);
        cbo_bank_prod = (Spinner)view.findViewById(R.id.cbo_bank_prod);

        initViews();

        inpAmount     = (EditText) view.findViewById(R.id.inpAmount);
        inpMemo       = (EditText) view.findViewById(R.id.inpMemo);

        btnLanjut   = (Button) view.findViewById(R.id.btnLanjut);
        btnLanjut.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                BankBean bankcode               = (BankBean) cbo_bank.getSelectedItem();
                BankProductBean bankcodeprod    = (BankProductBean) cbo_bank_prod.getSelectedItem();
                CommunityGWBean communityGWBean = (CommunityGWBean) cbo_community.getSelectedItem();
                MemberGWBean memberGWBean       = (MemberGWBean) cbo_member.getSelectedItem();

                member_id           = memberGWBean.member_id;
                member_code         = memberGWBean.member_code;
                bank_code           = String.valueOf(bankcode.bank_code);
                bank_name           = String.valueOf(bankcode.bank_name);
                currency            = String.valueOf(bankcode.ccy_id);
                amount              = inpAmount.getText().toString();
                remark              = inpMemo.getText().toString();
                Date cDate          = new Date();
                tx_date             = new SimpleDateFormat("yyyy-MM-dd").format(cDate);
                product_code        = String.valueOf(bankcodeprod.product_code);
                product_name        = String.valueOf(bankcodeprod.product_name);
                comm_code           = communityGWBean.getCode();
                comm_name           = communityGWBean.getName();

                if(amount.equalsIgnoreCase(""))
                {
                    Toast.makeText(getActivity(), R.string.form_alert, Toast.LENGTH_SHORT).show();
                }else{

                    pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, "Processing TopUp...");
                    AsyncHttpClient client = new AsyncHttpClient();
                    client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
                    client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
                    RequestParams params   = new RequestParams();

                    params.put("member_id", member_id);
                    params.put("ccy_id", currency);
                    params.put("amount", amount);
                    params.put("remark_topup", remark);
                    params.put("bank_code", bank_code);
                    params.put("product_code", product_code);

                    Log.d("params", params.toString());
                    client.post(AplConstants.TopUpValidateMobileAPI, params, new AsyncHttpResponseHandler() {
                        public void onSuccess(String content) {
                            Log.d("result:", content);
                            try {
                                JSONObject object         = new JSONObject(content);
                                String error_code         = object.getString("error_code");
                                String error_message      = object.getString("error_message");

                                if (pDialog != null) {
                                    pDialog.dismiss();
                                }

                                if (error_code.equals(AppParams.SUCCESS_CODE)) {
                                    JSONObject user_data = object.getJSONObject("trx_topup_data");
                                    tx_id        = user_data.getString("tx_id");
                                    tx_amount    = user_data.getString("tx_amount");
                                    BankBean bankcode            = (BankBean) cbo_bank.getSelectedItem();
                                    no_phone_source = String.valueOf(bankcode.member_phone);
                                    benef_acct_no   = String.valueOf(bankcode.benef_acct_no);
                                    benef_acct_name = String.valueOf(bankcode.benef_acct_name);
                                    charges_acct_no = String.valueOf(bankcode.charges_acct_no);
                                    fee_acct_no     = String.valueOf(bankcode.fee_acct_no);
                                    currency        = String.valueOf(bankcode.ccy_id);
                                    buyer_fee       = String.valueOf(bankcode.buyer_fee);
                                    seller_fee      = String.valueOf(bankcode.seller_fee);
                                    tx_fee          = String.valueOf(bankcode.tx_fee);

                                    String mStringArray[] = { tx_amount, tx_id };
                                    JSONArray mJSONArray  = new JSONArray(Arrays.asList(mStringArray));
                                    data_inv              = mJSONArray.toString();
                                    resend_token          = "N";

                                    pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, "Requesting Token TopUp...");
                                    AsyncHttpClient client = new AsyncHttpClient();
                                    client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
                                    client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
                                    RequestParams params   = new RequestParams();

                                    params.put("no_phone_source", no_phone_source);
                                    params.put("benef_acct_no", benef_acct_no);
                                    params.put("fee_acct_no", fee_acct_no);
                                    params.put("charges_acct_no", charges_acct_no);
                                    params.put("ccy_id", currency);
                                    params.put("buyer_fee", buyer_fee);
                                    params.put("seller_fee", seller_fee);
                                    params.put("tx_fee", tx_fee);
                                    params.put("data_inv", data_inv);
                                    params.put("resend_token", resend_token);

                                    client.post(AplConstants.RequestTokenEw9, params, new AsyncHttpResponseHandler() {
                                        public void onSuccess(String content) {
                                            try {

                                                JSONObject object         = new JSONObject(content);
                                                String error_code         = object.getString("error_code");
                                                String error_msg          = object.getString("error_message");

                                                if (pDialog != null) {
                                                    pDialog.dismiss();
                                                }

                                                if (error_code.equals(AppParams.SUCCESS_CODE)) {

                                                    JSONArray arrDataReqToken = object.getJSONArray("data");
                                                    String rq_trx_id = arrDataReqToken.getJSONObject(0).getString("trx_id");
                                                    String rq_date   = arrDataReqToken.getJSONObject(0).getString("trx_date");
                                                    String rq_desc   = arrDataReqToken.getJSONObject(0).getString("desc");

                                                    Fragment newFragment = null;
                                                    newFragment = new FragmentTopUpConfirmToken();
                                                    Bundle args = new Bundle();
                                                    args.putString("PaymentId", tx_id);
                                                    args.putString("MemberId", member_id);
                                                    args.putString("MemberCode", member_code);
                                                    args.putString("BankCode", bank_code);
                                                    args.putString("BankName", bank_name);
                                                    args.putString("ProductCode", product_code);
                                                    args.putString("ProductName", product_name);
                                                    args.putString("CommCode", comm_code);
                                                    args.putString("CommName", comm_name);
                                                    args.putString("BenefAccNo", benef_acct_no);
                                                    args.putString("BenefAccName", benef_acct_name);
                                                    args.putString("Amount", tx_amount);
                                                    args.putString("BuyerFee", buyer_fee);
                                                    args.putString("SellerFee", seller_fee);
                                                    args.putString("TxFee", tx_fee);
                                                    args.putString("ChargesAcctNo", charges_acct_no);
                                                    args.putString("FeeAcctNo", fee_acct_no);
                                                    args.putString("Remark", remark);
                                                    Integer TotalAmount = Integer.valueOf(tx_amount) + Integer.valueOf(buyer_fee) + Integer.valueOf(seller_fee);
                                                    args.putString("Total", String.valueOf(TotalAmount));
                                                    args.putString("Rq_trx_id", rq_trx_id);
                                                    args.putString("Rq_date", rq_date);
                                                    newFragment.setArguments(args);
                                                    switchFragment(newFragment);

                                                } else {
                                                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                                    alert.setTitle("Requesting Token TopUp");
                                                    alert.setMessage("Requesting Token Payment To Principal : " + error_msg);
                                                    alert.setPositiveButton("OK", null);
                                                    alert.show();
                                                }

                                            } catch (JSONException e) {
                                                // TODO Auto-generated catch block
                                                e.printStackTrace();
                                            }
                                        };


                                        public void onFailure(Throwable error, String content) {
                                            if (pDialog != null) {
                                                pDialog.dismiss();
                                            }
                                            Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                                        }
                                    });

                                } else {
                                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                    alert.setTitle("Payment To Principal");
                                    alert.setMessage("Payment To Principal : " + error_message);
                                    alert.setPositiveButton("OK", null);
                                    alert.show();
                                }

                            } catch (JSONException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        };


                        public void onFailure(Throwable error, String content) {
                            if (pDialog != null) {
                                pDialog.dismiss();
                            }
                            Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });
        return view;
    }

    private void initViews()
    {
        pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, "Requesting Community List...");

        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
        client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
        RequestParams params   = new RequestParams();

        String CustomerId = AppHelper.getCustomerId(getActivity());
        params.put("customer_id", CustomerId);
        Log.d("params", params.toString());
        client.post(AplConstants.CommunityListMobileAPI, params, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // TODO Auto-generated method stub
                super.onStart();
            }

            @Override
            public void onSuccess(String content) {
                // TODO Auto-generated method stub
                super.onSuccess(content);
                try {
                    if (pDialog != null) {
                        pDialog.dismiss();
                    }

                    JSONObject object = new JSONObject(content);
                    JSONArray array = object.getJSONArray("community_data");
                    int len = array.length();

                    community_id_arr     = new String[len];
                    community_name_arr   = new String[len];
                    community_code_arr   = new String[len];
                    buss_scheme_code_arr = new String[len];

                    for (int i = 0; i < len; i++) {
                        community_id_arr[i]       = array.getJSONObject(i).getString("community_id");
                        community_name_arr[i]     = array.getJSONObject(i).getString("community_name");
                        community_code_arr[i]     = array.getJSONObject(i).getString("community_code");
                        buss_scheme_code_arr[i]   = array.getJSONObject(i).getString("buss_scheme_code");
                        CommunityList.add(new CommunityGWBean(community_id_arr[i], community_name_arr[i], community_code_arr[i], buss_scheme_code_arr[i]));
                    }

                    CommunityGWAdapter cAdapter = new CommunityGWAdapter(getActivity(), android.R.layout.simple_spinner_item, CommunityList);
                    cbo_community.setAdapter(cAdapter);

                    cbo_community.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, "Requesting Member List...");

                            CommunityGWBean selectedCommunity = CommunityList.get(position);

                            MemberList.clear();
                            BankList.clear();
                            BankProductList.clear();

                            AsyncHttpClient client = new AsyncHttpClient();
                            client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
                            client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
                            RequestParams params   = new RequestParams();

                            String CustomerId = AppHelper.getCustomerId(getActivity());
                            params.put("customer_id", CustomerId);
                            params.put("community_id", selectedCommunity.getId());
                            Log.d("params", params.toString());

                            client.post(AplConstants.MemberListMobileAPI, params, new AsyncHttpResponseHandler() {
                                @Override
                                public void onSuccess(String content) {
                                    // TODO Auto-generated method stub
                                    super.onSuccess(content);
                                    Log.d("TAG", content);
                                    try {
                                        if (pDialog != null) {
                                            pDialog.dismiss();
                                        }

                                        JSONObject object = new JSONObject(content);
                                        JSONArray array = object.getJSONArray("member_data");
                                        int len = array.length();
                                        member_code_arr = new String[len];
                                        member_id_arr  = new String[len];
                                        for (int i = 0; i < len; i++){
                                            member_code_arr[i] = array.getJSONObject(i).getString("member_code");
                                            member_id_arr[i]   = array.getJSONObject(i).getString("member_id");
                                            MemberList.add(new MemberGWBean(member_id_arr[i], member_code_arr[i]));
                                        }

                                        MemberGWAdapter mAdapter = new MemberGWAdapter(getActivity(), android.R.layout.simple_spinner_item, MemberList);
                                        cbo_member.setAdapter(mAdapter);

                                        cbo_member.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
                                            @Override
                                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                                pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, "Requesting Bank List...");

                                                MemberGWBean selectedMember = MemberList.get(position);
                                                String MemberId = selectedMember.getMember_id();

                                                if(MemberId.equalsIgnoreCase(""))
                                                {
                                                    BankList.add(new BankBean("", "No Bank Data Found", "", "", "", "", "", "", "", "", "", ""));
                                                    BankAdapter bankAdapter = new BankAdapter(getActivity(), android.R.layout.simple_spinner_item, BankList);
                                                    cbo_bank.setAdapter(bankAdapter);

                                                    BankProductList.add(new BankProductBean("", "", "No Bank Product Found"));
                                                    BankProductAdapter bankproductAdapter = new BankProductAdapter(getActivity(), android.R.layout.simple_spinner_item, BankProductList);
                                                    cbo_bank_prod.setAdapter(bankproductAdapter);
                                                }else{

                                                    AsyncHttpClient client = new AsyncHttpClient();
                                                    client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
                                                    client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
                                                    RequestParams params   = new RequestParams();

                                                    params.put("member_id", MemberId);
                                                    Log.d("params", params.toString());

                                                    client.post(AplConstants.InquiryTopUpMobileAPI, params, new AsyncHttpResponseHandler() {
                                                        @Override
                                                        public void onSuccess(String content) {
                                                            // TODO Auto-generated method stub
                                                            super.onSuccess(content);
                                                            Log.d("TAG", content);
                                                            try {
                                                                if (pDialog != null) {
                                                                    pDialog.dismiss();
                                                                }

                                                                JSONObject object = new JSONObject(content);
                                                                JSONArray array = object.getJSONArray("bank_data");

                                                                int len = array.length();
                                                                bank_code_arr = new String[len];
                                                                bank_name_arr = new String[len];
                                                                ccy_id_arr = new String[len];
                                                                product_code_arr = new String[len];
                                                                seller_fee_arr = new String[len];
                                                                buyer_fee_arr = new String[len];
                                                                tx_fee_arr = new String[len];
                                                                member_phone_arr = new String[len];
                                                                benef_acct_no_arr = new String[len];
                                                                benef_acct_name_arr = new String[len];
                                                                charges_acct_no_arr = new String[len];
                                                                fee_acct_no_arr = new String[len];

                                                                for (int i = 0; i < len; i++){
                                                                    bank_code_arr[i]               = array.getJSONObject(i).getString("bank_code");
                                                                    bank_name_arr[i]               = array.getJSONObject(i).getString("bank_name");
                                                                    ccy_id_arr[i]                  = array.getJSONObject(i).getString("ccy_id");
                                                                    product_code_arr[i]            = array.getJSONObject(i).getString("product_code");
                                                                    seller_fee_arr[i]              = array.getJSONObject(i).getString("seller_fee");
                                                                    buyer_fee_arr[i]               = array.getJSONObject(i).getString("buyer_fee");
                                                                    tx_fee_arr[i]                  = array.getJSONObject(i).getString("tx_fee");
                                                                    member_phone_arr[i]            = array.getJSONObject(i).getString("member_phone");
                                                                    benef_acct_no_arr[i]           = array.getJSONObject(i).getString("benef_acct_no");
                                                                    benef_acct_name_arr[i]         = array.getJSONObject(i).getString("benef_acct_name");
                                                                    charges_acct_no_arr[i]         = array.getJSONObject(i).getString("charges_acct_no");
                                                                    fee_acct_no_arr[i]             = array.getJSONObject(i).getString("fee_acct_no");

                                                                    BankList.add(new BankBean(bank_code_arr[i], bank_name_arr[i], ccy_id_arr[i], product_code_arr[i], seller_fee_arr[i], buyer_fee_arr[i], tx_fee_arr[i], member_phone_arr[i], benef_acct_no_arr[i], benef_acct_name_arr[i], charges_acct_no_arr[i], fee_acct_no_arr[i]));
                                                                }

                                                                BankAdapter bankAdapter = new BankAdapter(getActivity(), android.R.layout.simple_spinner_item, BankList);
                                                                cbo_bank.setAdapter(bankAdapter);

                                                                cbo_bank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
                                                                    @Override
                                                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                                                        pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, "Requesting Bank Product...");

                                                                        BankBean selectedBank = BankList.get(position);

                                                                        BankProductList.clear();

                                                                        String BankCode = selectedBank.getBank_code();

                                                                        if(BankCode.equalsIgnoreCase(""))
                                                                        {
                                                                            BankProductList.add(new BankProductBean("", "", "No Bank Product Found"));
                                                                            BankProductAdapter bankproductAdapter = new BankProductAdapter(getActivity(), android.R.layout.simple_spinner_item, BankProductList);
                                                                            cbo_bank_prod.setAdapter(bankproductAdapter);
                                                                        }else{

                                                                            AsyncHttpClient client = new AsyncHttpClient();
                                                                            client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
                                                                            client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
                                                                            RequestParams params   = new RequestParams();

                                                                            params.put("bank_code", BankCode);
                                                                            Log.d("params", params.toString());

                                                                            client.post(AplConstants.BankProductMobileAPI, params, new AsyncHttpResponseHandler() {
                                                                                @Override
                                                                                public void onSuccess(String content) {
                                                                                    // TODO Auto-generated method stub
                                                                                    super.onSuccess(content);
                                                                                    Log.d("TAG", content);
                                                                                    try {
                                                                                        if (pDialog != null) {
                                                                                            pDialog.dismiss();
                                                                                        }

                                                                                        JSONObject object = new JSONObject(content);
                                                                                        JSONArray array = object.getJSONArray("bank_product_data");
                                                                                        int len = array.length();
                                                                                        bank_code_product_array    = new String[len];
                                                                                        product_code_array = new String[len];
                                                                                        product_name_array = new String[len];
                                                                                        for (int i = 0; i < len; i++){
                                                                                            bank_code_product_array[i]    = array.getJSONObject(i).getString("bank_code");
                                                                                            product_code_array[i]         = array.getJSONObject(i).getString("product_code");
                                                                                            product_name_array[i]         = array.getJSONObject(i).getString("product_name");

                                                                                            BankProductList.add(new BankProductBean(bank_code_product_array[i], product_code_array[i], product_name_array[i]));
                                                                                        }
                                                                                        BankProductAdapter bankproductAdapter = new BankProductAdapter(getActivity(), android.R.layout.simple_spinner_item, BankProductList);
                                                                                        cbo_bank_prod.setAdapter(bankproductAdapter);

                                                                                    } catch (JSONException e) {
                                                                                        // TODO Auto-generated catch block
                                                                                        e.printStackTrace();
                                                                                    }
                                                                                };


                                                                                public void onFailure(Throwable error, String content) {
                                                                                    if (pDialog != null) {
                                                                                        pDialog.dismiss();
                                                                                    }
                                                                                    Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                                                                                }
                                                                            });
                                                                        }
                                                                    }
                                                                    @Override
                                                                    public void onNothingSelected(AdapterView<?> parent) {}
                                                                });


                                                            } catch (JSONException e) {
                                                                // TODO Auto-generated catch block
                                                                e.printStackTrace();
                                                            }
                                                        };


                                                        public void onFailure(Throwable error, String content) {
                                                            if (pDialog != null) {
                                                                pDialog.dismiss();
                                                            }
                                                            Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                                                        }
                                                    });
                                                }
                                            }
                                            @Override
                                            public void onNothingSelected(AdapterView<?> parent) {}
                                        });


                                    } catch (JSONException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    }
                                };


                                public void onFailure(Throwable error, String content) {
                                    if (pDialog != null) {
                                        pDialog.dismiss();
                                    }
                                    Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {}
                    });

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            };


            public void onFailure(Throwable error, String content) {
                if (pDialog != null) {
                    pDialog.dismiss();
                }
                Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
            }
        });
    }

    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
        }

    };

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
    }
}

package sgo.mobile.mava.frameworks.security;

public interface IDecrypt {
    public String decrypt(String str);
}
